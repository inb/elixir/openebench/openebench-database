/**
 * *****************************************************************************
 * Copyright (C) 2023 Spanish National Bioinformatics Institute (INB) and
 * Barcelona Supercomputing Center
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 * *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.mongo.migration;

import com.mongodb.BasicDBObject;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoIterable;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.FindOneAndUpdateOptions;
import com.mongodb.client.result.DeleteResult;
import es.bsc.inb.elixir.openebench.mongo.MongoQueries;
import es.bsc.inb.elixir.openebench.mongo.OpenEBenchCollection;
import es.bsc.inb.elixir.openebench.mongo.OpenEBenchID;
import es.bsc.inb.elixir.openebench.mongo.OpenEBenchMeta;
import es.bsc.inb.elixir.openebench.validator.AbstractOEBSchemaResolver;
import es.elixir.bsc.json.schema.JsonSchemaLocator;
import es.elixir.bsc.json.schema.JsonSchemaReader;
import es.elixir.bsc.json.schema.ValidationError;
import es.elixir.bsc.json.schema.model.JsonSchema;
import es.elixir.bsc.json.schema.model.JsonStringSchema;
import es.elixir.bsc.json.schema.model.PrimitiveSchema;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.net.URI;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonString;
import javax.json.JsonValue;
import javax.json.stream.JsonGenerator;
import org.bson.BsonDateTime;
import org.bson.BsonDocument;
import org.bson.BsonInt32;
import org.bson.BsonNumber;
import org.bson.BsonString;
import org.bson.BsonType;
import org.bson.BsonValue;

/**
 * @author Dmitry Repchevsky
 */

public class Migrator {

    private final MongoDatabase tgt_db;
    private final MongoDatabase src_db;
    
    private final AbstractOEBSchemaResolver resolver;
    private final Map<String, BsonDocument> objects2move;
    private final Map<String, String> identifiers;
    
    private final JsonSchemaReader reader;
    
    private final String orcid;
    private final boolean keep_prov;
    private final boolean dryrun;

    public Migrator(MongoDatabase tgt_db, MongoDatabase src_db, 
            AbstractOEBSchemaResolver resolver, String orcid, 
            boolean keep_prov, boolean dryrun) {

        this.tgt_db = tgt_db;
        this.src_db = src_db;
        this.resolver = resolver;
        
        objects2move = new HashMap<>();
        identifiers = new HashMap<>();
        
        reader = JsonSchemaReader.getReader();
        
        this.orcid = orcid;
        this.keep_prov = keep_prov;
        this.dryrun = dryrun;
    }

    public String migrate() {
        return migrate(new StringWriter());
    }

    public String migrate(final Writer errors) {
    
        final JsonGenerator generator = Json.createGenerator(errors);
        try {
            generator.writeStartArray();
            final MongoIterable<String> collections = src_db.listCollectionNames();

            for (String collection: collections) {
                if (!collect(collection, generator)) {
                    return null;
                }
            }
            
            if (!move(generator)) {
                return null;
            }
        } finally {
            generator.writeEnd();
            generator.close();
        }
        
        final StringWriter writer = new StringWriter();
        try (writer;
             JsonGenerator gen = Json.createGenerator(writer)) {
        
            gen.writeStartArray();
            for (Map.Entry<String, BsonDocument> entry : objects2move.entrySet()) {
                final BsonDocument obj = entry.getValue();
                final BsonValue id = obj.get("_id");
                final BsonString orig_id = obj.getString("orig_id");
                
                gen.writeStartObject();
                gen.write("_id", OpenEBenchID.getOpenEBenchID(id));
                gen.write("_schema", obj.getString("_schema", new BsonString("")).getValue());
                gen.write("input_id", entry.getKey());
                if (orig_id != null) {
                    gen.write("orig_id", orig_id.getValue());
                }
                gen.writeEnd();
            }
            gen.writeEnd();
        } catch (IOException ex) {
            Logger.getLogger(Migrator.class.getName()).log(Level.SEVERE, null, ex);
        }
        return writer.toString();
    }

    private boolean move(final JsonGenerator out) {
        final List<Entry<String, BsonDocument>> entries = new ArrayList(objects2move.entrySet());
        
        for (int i = 0; i < 10 && entries.size() > 0; i++) {
            final Iterator<Entry<String, BsonDocument>> iterator = entries.iterator();

            while (iterator.hasNext()) {
                final Entry<String, BsonDocument> entry = iterator.next();

                final String old_id = entry.getKey();
                final BsonDocument doc = entry.getValue();
                
                final String collection = collection(entry);
                if (collection == null) {
                    Logger.getLogger(Migrator.class.getName()).log(Level.SEVERE, "Cant induce the collection for {0}", old_id);
                    continue;
                }
                
                try {
                    final MongoCollection<BsonDocument> target = tgt_db.getCollection(collection, BsonDocument.class);
                    final MongoCollection<BsonDocument> source = src_db.getCollection(collection, BsonDocument.class);

                    if (!dryrun) {
                        final BsonDocument prov = doc.getDocument(OpenEBenchMeta.PROVENANCE_PROPERTY);
                        if (prov != null) {
                            prov.remove(OpenEBenchMeta.UPDATED_PROPERTY);
                            prov.remove(OpenEBenchMeta.CREATED_PROPERTY);
                            
                            if (keep_prov) {
                                prov.remove(OpenEBenchMeta.ORCID_PROPERTY);
                            }
                        }
                        
                        final BsonValue _id = doc.get("_id");
                        BasicDBObject bson = new BasicDBObject("$set", doc)
                            .append("$currentDate", new BasicDBObject(
                                    OpenEBenchMeta.PROVENANCE_PROPERTY + "." + OpenEBenchMeta.UPDATED_PROPERTY, true))
                            .append("$setOnInsert", new BasicDBObject(
                                    OpenEBenchMeta.PROVENANCE_PROPERTY + "." + OpenEBenchMeta.CREATED_PROPERTY, new BsonDateTime(System.currentTimeMillis())));
                        
                        if (keep_prov) {
                            bson = bson.append("$setOnInsert", new BasicDBObject(
                                    OpenEBenchMeta.PROVENANCE_PROPERTY + "." + OpenEBenchMeta.ORCID_PROPERTY, orcid));
                        }
                        target.findOneAndUpdate(Filters.eq("_id", _id), bson, new  FindOneAndUpdateOptions().upsert(true));

                        final DeleteResult result = source.deleteOne(Filters.eq("_id", old_id));
                        if (result.getDeletedCount() == 0) {
                            Logger.getLogger(Migrator.class.getName()).log(Level.WARNING, "failed to remove provisional {0}", old_id);
                        }
                    }

                    iterator.remove();
                } catch (Exception ex) {
                    Logger.getLogger(Migrator.class.getName()).log(Level.SEVERE, "ERROR while migrating {0} to {1} {2}", new String[] {old_id, doc.getString("_id").getValue(), ex.getMessage()});
                }
            }
        }
        
        if (!entries.isEmpty()) {
            Logger.getLogger(Migrator.class.getName()).log(Level.SEVERE, "ERRORS occured while migrationg data!");
            return false;
        }
        
        return true;
    }

    private boolean collect(final String collection, final JsonGenerator out) {
        
        boolean ok = true;
        
        final MongoCollection<BsonDocument> colls = src_db.getCollection(collection, BsonDocument.class);
        final FindIterable<BsonDocument> docs = colls.find(
                Filters.eq(OpenEBenchMeta.PROVENANCE_PROPERTY + "." + OpenEBenchMeta.ORCID_PROPERTY, orcid));
        for (BsonDocument doc : docs) {
            final String _id = doc.getString("_id").getValue();
            if (!objects2move.containsKey(_id)) {
                ok &= migrate(doc, out) != null;
            }
        }
        
        return ok;
    }

    /**
     * Migrates the document from provisional database to the production one.
     * 
     * @param src_doc
     * 
     * @return 
     */
    private BsonDocument migrate(BsonDocument src_doc, final JsonGenerator out) {

        final String _id = src_doc.getString("_id").getValue();
        if (objects2move.containsKey(_id)) {
            return objects2move.get(_id);
        }
        
        final BsonString orig_id = src_doc.getString("orig_id", null);
        
        String collection = collection(_id, src_doc);

        JsonSchemaLocator locator;
        JsonSchema schema;
        try {
            locator = resolver.getJsonSchemaLocator(collection);
            if (locator == null) {
                throw new Exception();
            }
            schema = reader.read(locator);
        } catch (Exception ex) {
            Logger.getLogger(Migrator.class.getName()).log(Level.SEVERE, "no json schema for {0} found.", collection);
            out.writeStartObject();
            out.write("input_id", _id);
            out.write("_schema", src_doc.getString("_schema", new BsonString("")).getValue());

            if (orig_id != null) {
                out.write("orig_id", orig_id.getValue());
            }

            out.writeStartArray("errors");
            out.writeStartObject();
            out.write("message", String.format("no json schema for %s found.", collection));
            out.writeEnd();
            out.writeEnd();
            out.writeEnd();

            return null;
        }

        BsonValue new_id = null;
        if (orig_id != null) {
            try {
                final MongoCollection<BsonDocument> col = tgt_db.getCollection(collection, BsonDocument.class);
                final FindIterable<BsonDocument> iter = col.find(Filters.eq("orig_id", orig_id));
                final BsonDocument old = iter.first();
                if (old != null) {
                    // compare 'sandbox.@created' with 'staged.@updated' times
                    final BsonDateTime old_time = old.getDateTime(OpenEBenchMeta.UPDATED_PROPERTY, null);
                    final BsonDateTime new_time = src_doc.getDateTime(OpenEBenchMeta.CREATED_PROPERTY, null);
                    if (new_time == null || (old_time != null && new_time.compareTo(old_time) < 0)) {
                        Logger.getLogger(Migrator.class.getName()).log(Level.INFO, "rejected update for the outdated entry: {0}", orig_id);
                        out.writeStartObject();
                        out.write("_schema", old.getString("_schema", new BsonString("")).getValue());
                        out.write("input_id", _id);
                        out.write("orig_id", orig_id.getValue());
                        out.writeStartArray("errors");
                        out.writeStartObject();
                        out.write("message", String.format("rejected update for the outdated entry: %s", orig_id.getValue()));
                        out.writeEnd();
                        out.writeEnd();
                        out.writeEnd();
                        return null;
                    }

                    final BsonValue pk = old.get("_id");
                    final BsonType type = pk.getBsonType();
                    switch(type) {
                        case STRING: ; new_id = pk; break;
                        case DOCUMENT:
                            final String id = pk.asDocument().getString("id").getValue();
                            final BsonNumber revision = pk.asDocument().getNumber("revision");
                            new_id = new BsonDocument("id", new BsonString(id)).append("revision", new BsonInt32(revision == null ? 1 : revision.intValue() + 1));
                            Logger.getLogger(Migrator.class.getName()).log(Level.INFO, "new revision for the object {0}.", id);
                    }
                }
            } catch(Exception ex) {
                Logger.getLogger(Migrator.class.getName()).log(Level.SEVERE, ex.getMessage());
                out.writeStartObject();
                out.write("_schema", src_doc.getString("_schema", new BsonString("")).getValue());
                out.write("input_id", _id);
                out.write("orig_id", orig_id.getValue());
                out.writeStartArray("errors");
                out.writeStartObject();
                out.write(String.format("exception while migrating %s %s", _id, ex.getMessage()));
                out.writeEnd();
                out.writeEnd();
                out.writeEnd();
                return null;
            }
        }
        
        if (new_id == null) {
            new_id = id(_id, collection);
        }

        objects2move.put(_id, src_doc);
        
        final List<ValidationError> errors = new ArrayList<>();
        schema.validate(src_doc, errors, (PrimitiveSchema model, String pointer, BsonValue value, BsonValue parent, List<ValidationError> err) -> {
            
            if (model instanceof JsonStringSchema && value.isString()) {
                try {
                    final JsonValue object = locator.getSchema(model.getId(), model.getJsonPointer());
                    if (object == null) {
                        err.add(new ValidationError(model.getId(), model.getJsonPointer(), "no schema object found."));
                    } else if (object.getValueType() == JsonValue.ValueType.OBJECT) {
                        final JsonValue fkValue = object.asJsonObject().get("foreign_keys");
                        if (fkValue != null && JsonValue.ValueType.ARRAY == fkValue.getValueType()) {
                            String fk_collection = null;
                            final JsonArray fkArray = fkValue.asJsonArray();
                            for (int i = 0; i < fkArray.size(); i++) {
                                final JsonValue v = fkArray.get(i);
                                if (v.getValueType() == JsonValue.ValueType.OBJECT) {
                                    final JsonValue schema_id = v.asJsonObject().get("schema_id");
                                    if (schema_id != null && JsonValue.ValueType.STRING == schema_id.getValueType()) {
                                        fk_collection = ((JsonString)schema_id).getString();
                                        break;
                                    }
                                }
                            }
                            final String fk_value = value.asString().getValue();

                            if (fk_value.length() > 7 && fk_value.startsWith("OEB")) {
                                if (fk_collection == null) {
                                    fk_collection = OpenEBenchCollection.collection(fk_value); // try to guess
                                }
                                if (fk_collection == null) {
                                    err.add(new ValidationError(model.getId(), model.getJsonPointer(), "invalid FK: '" + fk_value + "'"));
                                } else {
                                    if (fk_value.charAt(7) == 't') {
                                        try {
                                            final MongoCollection<BsonDocument> colls = src_db.getCollection(fk_collection, BsonDocument.class);
                                            final BsonDocument fk_object = colls.find(
                                                    Filters.and(Filters.eq("_id", fk_value), Filters.eq(
                                                            OpenEBenchMeta.PROVENANCE_PROPERTY + "." + OpenEBenchMeta.ORCID_PROPERTY, orcid))).first();
                                            if (fk_object == null) {
                                                err.add(new ValidationError(model.getId(), model.getJsonPointer(), "no referenced object found: '" + fk_value + "'"));
                                            } else {
                                                final BsonDocument ref_object = migrate(fk_object, out);
                                                if (ref_object != null) {
                                                    switch(parent.getBsonType()) {
                                                        case DOCUMENT: 
                                                            for(Map.Entry<String, BsonValue> entry : parent.asDocument().entrySet()) {
                                                                if (value == entry.getValue()) {
                                                                    entry.setValue(new BsonString(
                                                                            OpenEBenchID.getOpenEBenchID(ref_object.get("_id"))));
                                                                }
                                                            }
                                                    }
                                                } else {
                                                    err.add(new ValidationError(model.getId(), model.getJsonPointer(), "error migrating referenced object: '" + fk_value + "'"));
                                                }
                                            }
                                        } catch(Exception ex) {
                                            err.add(new ValidationError(model.getId(), model.getJsonPointer(), "mongodb error: '" + ex.getMessage() + "'"));
                                        }
                                    } else {
                                       try {
                                           if (!validateFK(fk_collection, fk_value)) {
                                               err.add(new ValidationError(model.getId(), model.getJsonPointer(), "no referenced object found: '" + fk_value + "'"));
                                           }
                                        } catch(Exception ex) {
                                            err.add(new ValidationError(model.getId(), model.getJsonPointer(), "mongodb error: '" + ex.getMessage() + "'"));
                                        }
                                    }
                                }
                            } else {
                                try {
                                    if (!validateFK(fk_collection, fk_value)) {
                                        err.add(new ValidationError(model.getId(), model.getJsonPointer(), "no referenced object found: '" + fk_value + "'"));
                                    }
                                 } catch(Exception ex) {
                                     err.add(new ValidationError(model.getId(), model.getJsonPointer(), "mongo error: '" + ex.getMessage() + "'"));
                                 }                            
                            }
                        }
                    }
                } catch(IOException ex) {
                    err.add(new ValidationError(model.getId(), model.getJsonPointer(), "no schema object found."));
                }
            }
        });

        if (!errors.isEmpty()) {
            out.writeStartObject();
            out.write("_schema", src_doc.getString("_schema", new BsonString("")).getValue());
            out.write("input_id", _id);
            if (orig_id != null) {
                out.write("orig_id", orig_id.getValue());
            }
            out.writeStartArray("errors");
            for (ValidationError error : errors) {
                out.writeStartObject();
                out.write("pointer", error.pointer);
                out.write("path", error.path);
                out.write("message", error.message);
                out.writeEnd();
            }
            out.writeEnd();
            out.writeEnd();
            return null;
        }

        src_doc.append("_id", new_id);

        return src_doc;
    }
    
    private boolean validateFK(final String fk_collection, final String fk_value) {

        final MongoCollection<BsonDocument> src_colls = src_db.getCollection(fk_collection, BsonDocument.class);
        if (src_colls != null && src_colls.countDocuments(
                Filters.and(Filters.eq("_id", fk_value), 
                        Filters.eq(OpenEBenchMeta.PROVENANCE_PROPERTY + "." + OpenEBenchMeta.ORCID_PROPERTY, orcid))) > 0) {
            return true;
        }        

        final MongoCollection<BsonDocument> rgt_colls = tgt_db.getCollection(fk_collection, BsonDocument.class);
        if (rgt_colls != null && MongoQueries.hasDocument(rgt_colls, fk_value)) {
            return true;
        }
        
        return false;
    }

    /**
     * Get the final identifier for the temporal one.
     * 
     * @param _id
     * @param  collection
     * @return 
     */
    private BsonValue id(final String _id, String collection) {

        switch(collection) {
            case OpenEBenchCollection.CONTACT:
            case OpenEBenchCollection.PRIVILEGE:
            case OpenEBenchCollection.COMMUNITY:
            case OpenEBenchCollection.REFERENCE: return new BsonString(_id);
        }

        if (_id.length() < 7 || !_id.startsWith("OEB")) {
            return new BsonString(_id); // keep non-oeb identifiers intact
        }

        final String prefix = _id.substring(0, 7);
        
        String identifier = identifiers.get(prefix);
        if (identifier != null) {
            identifier = OpenEBenchID.incOpenEBenchID(identifier);
        } else {

            final MongoCollection<BsonDocument> col = tgt_db.getCollection(collection, BsonDocument.class);
            final BsonDocument last = col.find(Filters.regex("_id.id", "^" + prefix)).sort(Filters.eq("_id.id", -1)).first();

            if (last != null) {
                final BsonValue pk = last.get("_id");
                final String last_id;
                if (pk.isString()) {
                    last_id = pk.asString().getValue();
                } else if (pk.isDocument()) {
                    last_id = pk.asDocument().getString("id").getValue();
                } else {
                    Logger.getLogger(Migrator.class.getName()).log(Level.WARNING, "invalid primary key: {0}", pk);
                    return null; // ???
                }
                identifier = OpenEBenchID.incOpenEBenchID(last_id);
            } else {
                identifier = _id.substring(0, 7) + "0000000";
            }
        }
        identifiers.put(prefix, identifier);
        
        return new BsonDocument("id", new BsonString(identifier)).append("revision", new BsonInt32(1));
    }
    
    private String collection(final Entry<String, BsonDocument> entry) {
        final String _id = entry.getKey();
        final BsonDocument doc = entry.getValue();
        
        return collection(_id, doc);
    }

    private String collection(final String _id, final BsonDocument doc) {
        final BsonString _schema = doc.getString("_schema");
        if (_schema != null) {
            try {
                final URI uri = URI.create(_schema.getValue());
                return Paths.get(uri.getPath()).getFileName().toString();
            } catch (IllegalArgumentException ex) {
                Logger.getLogger(Migrator.class.getName()).log(Level.WARNING, "invalid '_schema' value {0}.", _schema.getValue());
            }
        }
        return OpenEBenchCollection.collection(_id);
    }
}
