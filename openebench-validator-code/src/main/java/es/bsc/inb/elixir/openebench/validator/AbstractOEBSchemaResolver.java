/**
 * *****************************************************************************
 * Copyright (C) 2021 Spanish National Bioinformatics Institute (INB) and
 * Barcelona Supercomputing Center
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 * *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.validator;

import es.elixir.bsc.json.schema.JsonSchemaLocator;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Dmitry Repchevsky
 */

public abstract class AbstractOEBSchemaResolver {
    
    private final Map<String, JsonSchemaLocator> locators;
    
    public AbstractOEBSchemaResolver() {
        locators = new ConcurrentHashMap<>();
    }
    
    public JsonSchemaLocator getJsonSchemaLocator(String collection) throws URISyntaxException, MalformedURLException {
        JsonSchemaLocator locator = locators.get(collection);
        if (locator == null) {
            locator = resolve(collection);
            if (locator != null) {
                locators.put(collection, locator);
            }
        }
        return locator;
    }
    
    public abstract JsonSchemaLocator resolve(String collection) throws URISyntaxException , MalformedURLException;
}
