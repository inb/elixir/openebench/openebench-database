/**
 * *****************************************************************************
 * Copyright (C) 2022 Spanish National Bioinformatics Institute (INB) and
 * Barcelona Supercomputing Center
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.validator;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoIterable;
import com.mongodb.client.model.Filters;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import es.bsc.inb.elixir.openebench.mongo.MongoQueries;
import es.bsc.inb.elixir.openebench.mongo.OpenEBenchCollection;
import es.bsc.inb.elixir.openebench.mongo.OpenEBenchID;
import es.elixir.bsc.json.schema.JsonSchemaException;
import es.elixir.bsc.json.schema.JsonSchemaReader;
import es.elixir.bsc.json.schema.ValidationError;
import es.elixir.bsc.json.schema.ValidationException;
import es.elixir.bsc.json.schema.model.JsonArraySchema;
import es.elixir.bsc.json.schema.model.JsonObjectSchema;
import es.elixir.bsc.json.schema.model.JsonProperties;
import es.elixir.bsc.json.schema.model.JsonSchema;
import es.elixir.bsc.json.schema.model.JsonSchemaElement;
import es.elixir.bsc.json.schema.model.JsonStringSchema;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Stream;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonValue;
import org.bson.BsonDocument;
import org.bson.BsonString;
import org.bson.BsonValue;
import org.bson.Document;

/**
 * @author Dmitry Repchevsky
 */

public class ObjectDependencyValidator {

    private final MongoDatabase staged;
    
    private final JsonSchemaReader reader;
    private final Map<String, Map<String, Map<String, JsonSchema>>> filters;
    
    public ObjectDependencyValidator(MongoDatabase staged) throws ValidationException {
        this(staged, new OpenEBenchSchemas());
    }

    public ObjectDependencyValidator(MongoDatabase staged, URI schema_location) 
            throws ValidationException {
        this(staged, new OpenEBenchSchemas(schema_location));
    }
    
    private ObjectDependencyValidator(MongoDatabase staged, OpenEBenchSchemas schemas) 
            throws ValidationException {

        this.staged = staged;
        
        reader = JsonSchemaReader.getReader();
        filters = new ConcurrentHashMap<>();
        
        final MongoIterable <String> collections = staged.listCollectionNames();
        for (String collection: collections) {
            if (OpenEBenchCollection.isOpenEBenchCollection(collection)) {
                try {
                    final GitHubSchemaLocator locator = schemas.getJsonSchemaLocator(collection);
                    if (locator == null) {
                        throw new ValidationException(
                            new ValidationError("no json schema for " + collection + " found."));
                    }
                    final JsonSchema schema = reader.read(locator);
                    final Stream<JsonSchema> children = schema.getChildren();
                    final Iterator<JsonSchema> iter = children.iterator();
                    while (iter.hasNext()) {
                        final JsonSchema subschema = iter.next();
                        final JsonValue value = locator.getSchema(subschema.getId(), subschema.getJsonPointer());
//                        System.out.println("------> " + collection + "." + subschema.getJsonPointer() + " => " + value);
                        if (value != null && value.getValueType() == JsonValue.ValueType.OBJECT) {
                            final JsonObject object = value.asJsonObject();
                            final JsonValue fkValue = object.get("foreign_keys");
                            if (fkValue != null && JsonValue.ValueType.ARRAY == fkValue.getValueType()) {
                                final JsonArray fkeys = fkValue.asJsonArray();
                                for (int i = 0, n = fkeys.size(); i < n; i++) {
                                    final JsonValue v = fkeys.get(i);
                                    if (JsonValue.ValueType.OBJECT == v.getValueType()) {
                                        final String fk_collection = v.asJsonObject().getString("schema_id", null);
//                                        System.out.println("======> " + collection + "." + subschema.getJsonPointer() + " => " + fk_collection);
                                        addFilter(collection, fk_collection, subschema);
                                    }
                                }
                            }
                        }
                    }
                } catch (IOException | JsonSchemaException | URISyntaxException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }
      
    /**
     * Add MongoDB search filter that searches a foreign key defined by the 
     * 'subschema' in the 'tgt' collection.
     * 
     * @param src collection that contains the foreign key
     * @param tgt collection which is referred by the foreign key
     * @param subschema json schema that defines the foreign key
     */
    private void addFilter(String src, String tgt, JsonSchema subschema) {
        Map<String, Map<String, JsonSchema>> map = filters.get(tgt);
        if (map == null) {
            filters.put(tgt, map = new HashMap());
        }
        
        Map<String, JsonSchema> queries = map.get(src);
        if (queries == null) {
            map.put(src, queries = new HashMap());
        }
        final String filter = makeFilter(subschema);
        if (filter != null) {
            queries.put(filter, subschema);
        }
    }
    
    private String makeFilter(JsonSchema subschema) {
        if (subschema instanceof JsonStringSchema) {
            return makeFilter(subschema.getParent(), subschema, null);
        }
        return null; // should be an error as fks in oeb schemas are always strings..
    }

    private String makeFilter(JsonSchemaElement schema, JsonSchemaElement child, String query) {
        
        if (schema == null) {
            return query; // reach the top.
        }
        
        if (schema instanceof JsonObjectSchema) {
            return makeFilter(schema.getParent(), schema, query);
        }
        
        if (schema instanceof JsonProperties) {
            final JsonProperties properties = (JsonProperties)schema;
            for (Iterator<Map.Entry<String, JsonSchema>> iter = properties.iterator(); iter.hasNext();) {
                final Map.Entry<String, JsonSchema> entry = iter.next();
                final JsonSchema property = entry.getValue();
                if (contains(property, child)) {
                    final String name = entry.getKey();
                    return makeFilter(schema.getParent(), schema, query == null ? name : name + "." + query);
                }
            }
            return null; // should never happen
        }
        if (schema instanceof JsonArraySchema) {
            return makeFilter(schema.getParent(), child, query);
        }
        
        return null;
    }
    
    private boolean contains(JsonSchemaElement schema, JsonSchemaElement element) {
        if (schema == element) {
            return true;
        }
        if (schema instanceof JsonArraySchema) {
            final JsonArraySchema array = (JsonArraySchema)schema;
            for (JsonSchema item : array.getItems()) {
                if (contains(item, element)) {
                    return true;
                }
            }
            return contains (array.getAdditionalItems(), element);
        }
        
        return false;
    }

    public Set<String> remove(final String openebench_id) {
        final String collection = findCollection(openebench_id);
        if (collection == null) {
            return null;
        }
        final LinkedHashMap<String, BsonDocument> uris = new LinkedHashMap();
        remove(collection, openebench_id, null, uris);
        return uris.keySet();
    }

    private void remove(final String collection, final String openebench_id, BsonDocument document, LinkedHashMap<String, BsonDocument> identifiers) {
        final String identifier = "/" + collection + "/" + openebench_id;
        if (!identifiers.containsKey(identifier)) {
            identifiers.put(identifier, document);
            if (document == null) {
                final List<DependencyValidationError> errors = new ArrayList();
                final List<BsonDocument> dependencies = getDependencies(collection, openebench_id, errors);

                if (dependencies != null) {
                    for (BsonDocument doc : dependencies) {
                        final String id = OpenEBenchID.getOpenEBenchID(doc.get("_id", new BsonString("")));
                        String col = null;
                        final BsonString schema = doc.getString("_schema");
                        if (schema != null) {
                            try {
                                final URI uri = URI.create(schema.getValue());
                                col = Paths.get(uri.getPath()).getFileName().toString();
                            } catch (IllegalArgumentException ex) {
                                col = findCollection(openebench_id);
                            }
                        }
                        if (col != null) {
                            remove(col, id, checkForArray(openebench_id, filters.get(collection).get(col), doc), identifiers);
                        }
                    }
                }
            }
        }
    }

    /**
     * Remove provided identifiers from mongodb database.
     * 
     * @param identifiers the list of identifiers to delete in a form of '/COLLECTION/ID'
     * 
     * @return the list of identifiers that failed to be removed
     */    
    public List<String> remove(final List<String> identifiers) {
        if (identifiers == null || identifiers.isEmpty()) {
            return identifiers;
        }

        final String path = identifiers.get(0);
        final String collection = getCollection(path);
        final String openebench_id = path.substring(collection.length() + 2);
        final LinkedHashMap<String, BsonDocument> map = new LinkedHashMap();
        remove(collection, openebench_id, null, map);

        if (map.size() != identifiers.size()) {
            return identifiers;
        }
        
        for (ListIterator<String> iter = identifiers.listIterator(identifiers.size()); iter.hasPrevious();) {
            final String identifier = iter.previous();
            if (map.containsKey(identifier)) {
                final BsonDocument document = map.remove(identifier);
                if ((document == null && delete(identifier)) ||
                    (document != null && update(identifier, document))) {
                    iter.remove();
                    continue;    
                }
            }
            return identifiers;
        }
        
        return identifiers;
    }

    private boolean delete(final String path) {
        try {
            
            final String collection = getCollection(path);
            final String openebench_id = path.substring(collection.length() + 2);
            final MongoCollection<Document> mc = staged.getCollection(collection);
            if (mc != null) {
                final DeleteResult result = mc.deleteMany(Filters.or(
                        MongoQueries.getOpenEBenchIdFilter(openebench_id), 
                        Filters.eq("_id", openebench_id))); // support datamodel 1.0
                if (result.getDeletedCount() > 0) {
                    return true;
                }
            }
            
        } catch (Exception ex) {}
        
        return false;
    }

    private boolean update(final String identifier, final BsonDocument document) {
        try {
            final Path path = Paths.get(identifier);
            final String collection = path.getParent().getFileName().toString();
            final MongoCollection<BsonDocument> mc = staged.getCollection(collection, BsonDocument.class);
            if (mc != null) {
                final BsonValue _id = document.get("_id");
                final UpdateResult result = mc.replaceOne(Filters.eq("_id", _id), document);
                if (result.getModifiedCount() > 0) {
                    return true;
                }
            }
        } catch (Exception ex) {}
        
        return false;
    }
    
    /**
     * Method infers the collection either from the identifier itself or
     * trying to find the mongodb object iterating collections that do not follow
     * standard 'OEB' scheme (e.g.'Reference').
     * 
     * @param openebench_id openebench identifier
     * @param errors collectio to put errors in
     * 
     * @return list of OpenEBench objects that depends on the object with provided identifier 
     */
    public List<BsonDocument> getDependencies(final String openebench_id, final List<DependencyValidationError> errors) {
        final String collection = findCollection(openebench_id);
        if (collection != null) {
            return getDependencies(collection, openebench_id, errors);
        }
        return null;
    }
    
    public List<BsonDocument> getDependencies(final String collection, 
            final String openenebch_id, final List<DependencyValidationError> errors) {

        final Map<String, Map<String, JsonSchema>> map = filters.get(collection);
        if (map == null) {
            return null;
        }

        final Set<BsonDocument> set = new HashSet();
        for (Map.Entry<String, Map<String, JsonSchema>> entry : map.entrySet()) {
            final String src_coll = entry.getKey();
            final MongoCollection<BsonDocument> mc = staged.getCollection(src_coll, BsonDocument.class);
            for (Map.Entry<String, JsonSchema> e : entry.getValue().entrySet()) {
                final FindIterable<BsonDocument> iterable = mc.find(Filters.eq(e.getKey(), openenebch_id));
                try (MongoCursor<BsonDocument> cursor = iterable.cursor()) {
                    while(cursor.hasNext()) {
                        final BsonDocument doc = cursor.next();
                        set.add(doc);    
                        errors.add(new DependencyValidationError(
                                OpenEBenchID.getOpenEBenchID(doc.get("_id", new BsonString(""))), 
                                doc.getString("_schema", new BsonString("")).getValue(),
                                e.getValue().getId(),
                                e.getValue().getJsonPointer()));
                    }
                }
            }
        }
        return new ArrayList(set);
    }

    /**
     * 
     * @param openebench_id the reffered identifier to search
     * @param filters map of foregn keys to filter properties where search is performed
     * @param value BSON element to search reffered identifier in
     * 
     * @return null if object should be removed
     */
    private <T extends BsonValue> T checkForArray(final String openebench_id, 
            Map<String, JsonSchema> filters, final T value) {

        switch (value.getBsonType()) {
            case STRING: return openebench_id.equals(value.asString().getValue()) ? null : value;
            case DOCUMENT: 
                for (Map.Entry<String, BsonValue> entry : value.asDocument().entrySet()) {
                    final BsonValue property = checkForArray(openebench_id, filters, entry.getValue());
                    if (property == null) {
                        if (entry.getValue().isString()) {
                            final String property_name = entry.getKey();
                            for (String key : filters.keySet()) {
                                final int idx = key.lastIndexOf('.');
                                if (property_name.equals(idx < 0 ? key : key.substring(idx + 1))) {
                                    return null;
                                }
                            }
                        } else {
                            return null;
                        }
                    }
                }
                break;
            case ARRAY:
                if (!value.asArray().isEmpty()) {
                    for (Iterator<BsonValue> iter = value.asArray().iterator(); iter.hasNext();) {
                        final BsonValue item = iter.next();
                        final BsonValue val = checkForArray(openebench_id, filters, item);
                        if (val == null) {
//                            if (!item.isString()) { 
//                                return null;
//                            }
                            iter.remove();
                        }
                    }
                    if (value.asArray().isEmpty()) {
                        return null;
                    }
                }
        }
        return value;
    }

    private String findCollection(final String openebench_id) {
        final String collection = OpenEBenchCollection.collection(openebench_id);
        if (collection != null) {
            return collection;
        }

        if (MongoQueries.hasDocument(staged.getCollection(OpenEBenchCollection.REFERENCE), openebench_id)) {
            return OpenEBenchCollection.REFERENCE;
        }

        if (MongoQueries.hasDocument(staged.getCollection(OpenEBenchCollection.CONTACT), openebench_id)) {
            return OpenEBenchCollection.CONTACT;
        }

        if (MongoQueries.hasDocument(staged.getCollection(OpenEBenchCollection.PRIVILEGE), openebench_id)) {
            return OpenEBenchCollection.PRIVILEGE;
        }
        
        return null;
    }
    
    /**
     * Extract the collection from the path ("/collection/id")
     * @param path
     * 
     * @return collection or null 
     */
    private String getCollection(final String path) {
        if (path != null && path.length() > 3 && path.charAt(0) == '/') {
            final int idx = path.indexOf('/', 1);
            return idx > 1 ? path.substring(1, idx) : null;
        }
        return null;
    }
}
