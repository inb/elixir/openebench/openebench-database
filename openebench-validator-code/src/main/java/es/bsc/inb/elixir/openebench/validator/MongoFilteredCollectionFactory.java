/**
 * *****************************************************************************
 * Copyright (C) 2022 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 * *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.validator;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import es.bsc.inb.elixir.openebench.auth.AbstractAuthorization;
import es.bsc.inb.elixir.openebench.auth.CollectionAuthorizationFilter;
import es.bsc.inb.elixir.openebench.mongo.MongoQueries;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonString;
import javax.json.JsonValue;
import org.bson.Document;
import org.bson.conversions.Bson;

/**
 * Utility class to search collections using a json object filter.
 * 
 * @author Dmitry Repchevsky
 */

public class MongoFilteredCollectionFactory {

    public static CollectionAuthorizationFilter getCollectionIterator(
            final MongoCollection<Document> col, 
            final JsonObject filter, 
            final AbstractAuthorization authz) {

        try {
            if (col != null) {
                if (filter == null || filter.isEmpty()) {
                    return new CollectionAuthorizationFilter(authz, col.find());
                }

                List<Bson> filters = new ArrayList();
                setFilters(filters, null, filter);
                return new CollectionAuthorizationFilter(authz, col.find(Filters.and(filters)));
            }
        } catch(Exception ex) {
            Logger.getLogger(MongoFilteredCollectionFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private static void setFilters(List<Bson> filters, String key, JsonObject obj) {
        for (Entry<String, JsonValue> entry : obj.entrySet()) {
            final JsonValue value = entry.getValue();
            final String property = key == null ? entry.getKey() : key + "." + entry.getKey();
            switch (value.getValueType()) {
                case STRING: if ("_id".equals(property)) {
                                 filters.add(MongoQueries.getOpenEBenchIdFilter(((JsonString)value).getString()));
                             } else {
                                 filters.add(Filters.eq(property, ((JsonString)value).getString()));
                             }
                             break;
                case NUMBER: filters.add(Filters.eq(property, ((JsonNumber)value).numberValue())); break;
                case TRUE: filters.add(Filters.eq(property, true)); break;
                case FALSE: filters.add(Filters.eq(property, false)); break;
                case NULL: filters.add(Filters.eq(property, null)); break;
                case OBJECT: if ("_id".equals(property)) {
                                final String id = value.asJsonObject().getString("id");
                                final String rev = value.asJsonObject().getString("revision", null);
                                filters.add(MongoQueries.getOpenEBenchIdFilter(rev == null ? id : id + "." + rev)); break;
                             } else {
                                setFilters(filters, property, value.asJsonObject());
                             }
            }
        }
    }
}
