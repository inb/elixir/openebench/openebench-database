/**
 * *****************************************************************************
 * Copyright (C) 2022 Spanish National Bioinformatics Institute (INB) and
 * Barcelona Supercomputing Center
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.validator;

import es.elixir.bsc.json.schema.impl.DefaultJsonSchemaLocator;
import java.io.InputStream;
import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.JsonValue;

/**
 * @author Dmitry Repchevsky
 */

public class GitHubSchemaLocator extends DefaultJsonSchemaLocator {
    
    private final static String DEFAULT_URI = 
            "https://raw.githubusercontent.com/inab/benchmarking-data-model/2.0.x/json-schemas/2.0.x/";

    static {
        String uri = DEFAULT_URI;
        try {
            try (InputStream in = GitHubSchemaLocator.class.getClassLoader().getResourceAsStream("META-INF/config.properties")) {
                if (in != null) {
                    final Properties properties = new Properties();
                    properties.load(in);
                    uri = properties.getProperty("github.schema.location");
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(GitHubSchemaLocator.class.getName()).log(Level.SEVERE, null, ex);
        }
        GITHUB_SCHEMA_LOCATION = URI.create(uri);
    }

    public final static URI GITHUB_SCHEMA_LOCATION;
    
    public GitHubSchemaLocator(URI uri) {
        super(uri);
    }
    
    protected GitHubSchemaLocator(URI uri, Map<URI, JsonValue> schemas) {
        super(uri, schemas);
    }

    @Override
    public GitHubSchemaLocator resolve(URI uri) {
        final Path path = Paths.get(uri.getPath()).getFileName();
        final StringBuilder file = new StringBuilder(path.toString());
        file.setCharAt(0, Character.toLowerCase(file.charAt(0)));
        file.append(".json");
        return new GitHubSchemaLocator(super.uri.resolve(file.toString()), super.schemas);
    }
    
    public Map<URI, JsonValue> getSchemas() {
        return Collections.unmodifiableMap(schemas);
    }
}
