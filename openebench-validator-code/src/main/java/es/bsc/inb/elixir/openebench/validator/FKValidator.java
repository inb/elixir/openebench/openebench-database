/**
 * *****************************************************************************
 * Copyright (C) 2021 Spanish National Bioinformatics Institute (INB) and
 * Barcelona Supercomputing Center
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.validator;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoIterable;
import es.bsc.inb.elixir.openebench.mongo.MongoQueries;
import es.bsc.inb.elixir.openebench.mongo.OpenEBenchCollection;
import es.bsc.inb.elixir.openebench.mongo.OpenEBenchID;
import es.elixir.bsc.json.schema.JsonSchemaException;
import es.elixir.bsc.json.schema.JsonSchemaLocator;
import es.elixir.bsc.json.schema.JsonSchemaReader;
import es.elixir.bsc.json.schema.ValidationError;
import es.elixir.bsc.json.schema.model.JsonObjectSchema;
import es.elixir.bsc.json.schema.model.JsonSchema;
import es.elixir.bsc.json.schema.model.JsonStringSchema;
import es.elixir.bsc.json.schema.model.PrimitiveSchema;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonString;
import javax.json.JsonValue;
import org.bson.BsonDocument;
import org.bson.BsonString;
import org.bson.BsonValue;
import org.bson.Document;

/**
 * @author Dmitry Repchevsky
 */

public class FKValidator {
    private final MongoDatabase staged;
    
    private final JsonSchemaReader reader;
    private final Map<String, JsonSchemaLocator> locators;
    
    public FKValidator(final MongoDatabase staged) {
        this.staged = staged;
        
        reader = JsonSchemaReader.getReader();
        locators = new ConcurrentHashMap<>();
    }
    
    /**
     * Validates Foreign Keys in the database.
     * 
     * @param database database to validate
     * @param errors list to collect validation errors
     */
    public void validate(
            final MongoDatabase database, final List<ValidationError> errors) {
        MongoIterable <String> collections = database.listCollectionNames();
        for (String collection: collections) {
            if (OpenEBenchCollection.isOpenEBenchCollection(collection)) {
                validate(database, collection, errors);
            }
        }
    }
    
    /**
     * Validates JSON document over production and temporal databases
     * 
     */
    private void validate(final MongoDatabase database,
                          final String collection,
                          final List<ValidationError> errors) {
        
        JsonSchemaLocator locator;
        JsonSchema schema;
        try {
            locator = getJsonSchemaLocator(collection);
            if (locator == null) {
                errors.add(new ValidationError("no json schema for " + collection + " found."));
                return;
            }
            schema = reader.read(locator);
        } catch (JsonSchemaException | URISyntaxException | MalformedURLException ex) {
            errors.add(new ValidationError("error reading schema for " + collection + " : " + ex.getMessage()));
            return;
        }
        
        final MongoCollection<BsonDocument> colls = database.getCollection(collection, BsonDocument.class);
        FindIterable<BsonDocument> docs = colls.find();
        for (BsonDocument doc : docs) {
            final BsonValue pk = doc.get("_id");
            final String id = OpenEBenchID.getOpenEBenchID(pk);
            doc.append("_id", new BsonString(id));
            
            List<ValidationError> erz = new ArrayList<>();
            schema.validate(doc, erz, (PrimitiveSchema model, String pointer, BsonValue value, BsonValue parent, List<ValidationError> err) -> {
                try {
                    if (model instanceof JsonObjectSchema && 
                        "/".equals(model.getJsonPointer())) { // look only in ROOT schema(s)

                        final JsonValue object = locator.getSchema(model.getId(), model.getJsonPointer());
                        if (object.getValueType() == JsonValue.ValueType.OBJECT) {
                            JsonValue pkValue = object.asJsonObject().get("primary_key");
                            if (pkValue != null) {
                                if (JsonValue.ValueType.ARRAY == pkValue.getValueType()) {
                                    final Map<String, String> pkMap = new HashMap<>();
                                    final JsonArray pkeys = pkValue.asJsonArray();
                                    for (int i = 0, n = pkeys.size(); i < n; i++) {
                                        JsonValue pkey = pkeys.get(i);
                                        if (JsonValue.ValueType.STRING == pkey.getValueType()) {
                                            final String property = ((JsonString)pkey).getString();
                                            final BsonValue keyValue = value.asDocument().get(property);
                                            if (keyValue.isString()) {
                                                pkMap.put(property, keyValue.asString().getValue());
                                            }
                                        }
                                    }
                                    if (!validatePKs(database, pkMap, value.asDocument())) {
                                        err.add(new ValidationError(model.getId(), model.getJsonPointer(), "PK constraint violation: '" + pkMap + "'"));
                                    }
                                }
                            }
                        }
                    } else if (model instanceof JsonStringSchema && value.isString()) {
                        final JsonValue object = locator.getSchema(model.getId(), model.getJsonPointer());
                        if (object.getValueType() == JsonValue.ValueType.OBJECT) {
                            JsonValue fkValue = object.asJsonObject().get("foreign_keys");
                            if (fkValue != null && JsonValue.ValueType.ARRAY == fkValue.getValueType() && 
                                !validateFKs(staged, database, value.asString().getValue(), fkValue.asJsonArray())) {
                                err.add(new ValidationError(model.getId(), model.getJsonPointer(), " FK constraint violation: '" + 
                                        value.asString().getValue() + "'"));
                            }
                        }
                    }
                } catch(IOException ex) {
                    
                }

            });
            
            for (ValidationError err : erz) {
                errors.add(new ValidationError(err.id, err.pointer, collection + "[" + id + "] " + err.pointer + " " + err.message));
            }
        }

    }

    public boolean validatePKs(MongoDatabase db, Map<String, String> values, BsonDocument object) {
        final BsonString _schema = object.getString("_schema");
        final String[] path = _schema.getValue().split("/");
        final String collection = path[path.length - 1];
        
        for(String primary_key : values.values()) {
            if (hasId(db, collection, primary_key)) {
                return false;
            }
        }
        return true;
    }
    
    public boolean validateFKs(MongoDatabase staged, MongoDatabase sandbox, String value, JsonArray fkeys) {
        for (int i = 0, n = fkeys.size(); i < n; i++) {
            JsonValue fk_schema = fkeys.get(i);
            if (JsonValue.ValueType.OBJECT == fk_schema.getValueType()) {
                if (!validateFK(staged, sandbox, value, fk_schema.asJsonObject())) {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean validateFK(MongoDatabase staged, MongoDatabase sandbox, String value, JsonObject fk_schema) {

        final JsonValue _id = fk_schema.get("schema_id");
        if (_id == null) {
            return false;
        }
        if (JsonValue.ValueType.STRING != _id.getValueType()) {
            return false;
        }
        
        return hasId(staged, ((JsonString)_id).getString(), value) || 
               (sandbox != null && hasId(sandbox, ((JsonString)_id).getString(), value));
    }
    
    private boolean hasId(MongoDatabase db, String collection, String id) {
        MongoCollection<Document> col = db.getCollection(collection);
        return MongoQueries.hasDocument(col, id);
    }

    /**
     * Returns JsonSchemaLocator for the given mongodb collection
     * 
     * @param collection the name of mongodb collection
     * @return JsonSchemaLocator which locate Json schema document
     * 
     * @throws URISyntaxException 
     */
    private JsonSchemaLocator getJsonSchemaLocator(final String collection) 
            throws URISyntaxException , MalformedURLException {
        
        JsonSchemaLocator locator = locators.get(collection);
        if (locator == null) {
            //final URL url = FKValidator.class.getClassLoader().getResource(collection + ".json");
            StringBuilder file = new StringBuilder(collection);
            file.setCharAt(0, Character.toLowerCase(collection.charAt(0)));
            file.append(".json");

            final URL url = GitHubSchemaLocator.GITHUB_SCHEMA_LOCATION.resolve(file.toString()).toURL();
            if (url != null) {
                locators.put(collection, locator = new GitHubSchemaLocator(url.toURI()));
            }
        }
        return locator;
    }
}
