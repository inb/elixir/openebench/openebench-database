/**
 * *****************************************************************************
 * Copyright (C) 2021 Spanish National Bioinformatics Institute (INB) and
 * Barcelona Supercomputing Center
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.validator;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Dmitry Repchevsky
 */

public final class OpenEBenchSchemas {
    
    private final URI schema_location;
    private final Map<String, GitHubSchemaLocator> locators;
    
    public OpenEBenchSchemas() {
        this(GitHubSchemaLocator.GITHUB_SCHEMA_LOCATION);
    }
    
    public OpenEBenchSchemas(URI schema_location) {
        this.schema_location = schema_location;
        
        locators = new ConcurrentHashMap();
    }
    
    /**
     * Returns JsonSchemaLocator for the given mongodb collection
     * 
     * @param collection the name of mongodb collection
     * @return JsonSchemaLocator which locate Json schema document
     * 
     * @throws URISyntaxException 
     * @throws java.net.MalformedURLException 
     */
    public GitHubSchemaLocator getJsonSchemaLocator(String collection) 
            throws URISyntaxException , MalformedURLException {
        
        GitHubSchemaLocator locator = locators.get(collection);
        if (locator == null) {
            StringBuilder file = new StringBuilder(collection);
            file.setCharAt(0, Character.toLowerCase(collection.charAt(0)));
            file.append(".json");

            final URL url = schema_location.resolve(file.toString()).toURL();
            if (url != null) {
                locators.put(collection, locator = new GitHubSchemaLocator(url.toURI()));
            }
        }
        return locator;
    }
}
