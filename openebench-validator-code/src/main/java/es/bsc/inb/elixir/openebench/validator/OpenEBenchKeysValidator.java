/**
 * *****************************************************************************
 * Copyright (C) 2022 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.validator;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import es.bsc.inb.elixir.openebench.auth.Authorization;
import es.bsc.inb.elixir.openebench.mongo.MongoQueries;
import es.bsc.inb.elixir.openebench.mongo.OpenEBenchCollection;
import es.bsc.inb.elixir.openebench.mongo.OpenEBenchID;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonString;
import javax.json.JsonValue;
import javax.json.JsonValue.ValueType;
import org.bson.BsonDocument;
import org.bson.Document;

/**
 * @author Dmitry Repchevsky
 */

public class OpenEBenchKeysValidator {
    
    private final Authorization authorization;
    
    public OpenEBenchKeysValidator(Authorization authorization) {
        this.authorization = authorization;
    }
    
    public boolean validatePKs(final Map<String, String> values, 
                               final JsonObject object,
                               final String community,
                               final String username) {

        final JsonString _schema = object.getJsonString("_schema");
        final String[] path = _schema.getString().split("/");
        final String collection = path[path.length - 1];
        
        for(String primary_key : values.values()) {
            if (isValid(new StringBuilder(primary_key), collection, community, username, true)) {
                return false;
            }
        }
        return true;
    }
    
    public boolean validateFKs(final StringBuilder value, 
                               final JsonArray fkeys,
                               final String community,
                               final String username) {
        for (int i = 0, n = fkeys.size(); i < n; i++) {
            final JsonValue fk_schema = fkeys.get(i);
            if (ValueType.OBJECT == fk_schema.getValueType()) {
                if (!isValidFK(value, fk_schema.asJsonObject(), community, username)) {
                    return false;
                }
            } else {
                Logger.getLogger(OpenEBenchKeysValidator.class.getName()).log(Level.SEVERE, "foreign_key is not an object");
            }
        }
        return true;
    }

    private boolean isValidFK(final StringBuilder value, 
                              final JsonObject fk_schema,
                              final String community,
                              final String username) {

        final JsonValue _id = fk_schema.get("schema_id");
        if (_id == null) {
            Logger.getLogger(OpenEBenchKeysValidator.class.getName()).log(Level.SEVERE, "schema_id == null");
            return false;
        }
        if (ValueType.STRING != _id.getValueType()) {
            Logger.getLogger(OpenEBenchKeysValidator.class.getName()).log(Level.SEVERE, "schema_id must be STRING");
            return false;
        }
        
        return isValid(value, ((JsonString)_id).getString(), community, username, false); // pk = false
    }
    
    private boolean isValid(final StringBuilder value, 
                            final String collection, 
                            final String community,
                            final String username,
                            final boolean pk) {
        
        final String id = value.toString();

        final String _id = getId(community, collection, id, username, pk);
        if (_id == null) {
            return false;
        }

        if (!id.equals(_id)) {
            value.setLength(0);
            value.append(_id);
        }
        
        if (authorization.staged != null) {
            final MongoCollection<Document> colls = authorization.staged.getCollection(collection);
            if (colls != null && MongoQueries.hasDocument(colls, _id)) {
                return true;
            }
        }

        if (authorization.sandbox != null) {
            final MongoCollection<Document> colls = authorization.sandbox.getCollection(collection);
            if (colls != null && MongoQueries.hasDocument(colls, _id)) {
                return true;
            }
        }
        
        return false;
    }

    public String getPrimaryKey(final String community_id,
                                final String collection, 
                                final String id,
                                final String username) {
        return getId(community_id, collection, id, username, true);
    }
    
    private String getId(final String community_id,
                         final String collection, 
                         final String id,
                         final String username,
                         final boolean pk) {

        final char ch;
        switch(collection) {
            case OpenEBenchCollection.DATASET:            ch = 'D'; break;
            case OpenEBenchCollection.BENCHMARKING_EVENT: ch = 'E'; break;
            case OpenEBenchCollection.CHALLENGE:          ch = 'X'; break;
            case OpenEBenchCollection.METRICS:            ch = 'M'; break;
            case OpenEBenchCollection.METRICS_CATEGORY:   ch = 'Y'; break;
            case OpenEBenchCollection.TEST_ACTION:        ch = 'A'; break;
            case OpenEBenchCollection.TOOL:               ch = 'T'; break;
            case OpenEBenchCollection.COMMUNITY:
                                      if (id.startsWith("OEB") && id.length() == 7) {
                                          return id;
                                      }
                                      if (community_id != null && 
                                          community_id.matches("OEBC\\d\\d\\d")) {
                                          return community_id;
                                      }
                                      ch = 'C'; break;
            default: return id;
        }

        if (id.startsWith("OEB")) {
            return id.length() == 14 ? id : null;
        }
        
        if (community_id == null) {
            return null; // cant generate the id without the community code
        }
        
        final String community_code;
        if (ch == 'C') {
            community_code = "---";
        } else if (community_id.matches("OEBC\\d\\d\\d")) {
            community_code = community_id.substring(4);
        } else {
            return null;
        }

        if (authorization.sandbox != null) {
            // return temporal OEB identifier
            int hash = 7;
            for (int i = 0; i < id.length(); i++) {
                hash = hash * 31 + id.charAt(i);
            }
            for (int i = 0; i < username.length(); i++) {
                hash = hash * 31 + username.charAt(i);
            }
            
            final String code = Integer.toString(Math.abs(hash), 36).toUpperCase();
            
            return "OEB" + ch + community_code + "t" + "000000".substring(code.length()) + code;
        }
        
        return getStagedId(community_id, collection, ch, id, pk);
    }
    
    private String getStagedId(final String community_code,
                                   final String collection,
                                   final char ch,
                                   final String id,
                                   final boolean pk) {
        if (authorization.staged != null) {
            final MongoCollection<BsonDocument> colls = authorization.staged.getCollection(collection, BsonDocument.class);
            if (colls != null) {
            
                final BsonDocument doc = colls.find(Filters.eq("orig_id", id)).first();
                if (doc != null) {
                    return OpenEBenchID.getOpenEBenchID(doc.get("_id"));
                }
                
//                // generate new identifier only for the primary key
//                if (pk) {
//                    final Document last = colls.find(Filters.regex("_id", "^OEB" + ch + community_code)).sort(Filters.eq("_id", -1)).first();
//                    return last == null ? id.substring(0, 7) + "0000000" : OpenEBenchID.incOpenEBenchID(last.getString("_id"));
//                }
            }
        }
        return null;
    }

}
