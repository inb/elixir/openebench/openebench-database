/**
 * *****************************************************************************
 * Copyright (C) 2021 Spanish National Bioinformatics Institute (INB) and
 * Barcelona Supercomputing Center
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 * *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.validator;

import es.elixir.bsc.json.schema.JsonSchemaLocator;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

/**
 * @author Dmitry Repchevsky
 */

public class OpenEBenchSchemaResolver extends AbstractOEBSchemaResolver {

    private final URI github_schema_location;
    
    public OpenEBenchSchemaResolver() {
        github_schema_location = GitHubSchemaLocator.GITHUB_SCHEMA_LOCATION;
    }
    
    public OpenEBenchSchemaResolver(final String github_schema_location) {
        this.github_schema_location = URI.create(github_schema_location);
    }
    
    @Override
    public JsonSchemaLocator resolve(String collection) throws URISyntaxException , MalformedURLException {
        StringBuilder file = new StringBuilder(collection);
        file.setCharAt(0, Character.toLowerCase(collection.charAt(0)));
        file.append(".json");

        final URL url = github_schema_location.resolve(file.toString()).toURL();

        return url == null ? null : new GitHubSchemaLocator(url.toURI());
    }
    
}
