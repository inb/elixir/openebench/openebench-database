/**
 * *****************************************************************************
 * Copyright (C) 2021 Spanish National Bioinformatics Institute (INB) and
 * Barcelona Supercomputing Center
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.mongo.migration;

import com.mongodb.ConnectionString;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;
import es.bsc.inb.elixir.openebench.validator.OpenEBenchSchemaResolver;
import java.io.BufferedWriter;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Dmitry Repchevsky
 */

public class MigrationTool {

    private final static String HELP = "openebench-migration-tool [-host, -port, -u, -p, -out, -dry] -source -target -prov\n\n" +
                                       "parameters:\n\n" +
                                       "-host              - mongodb host (default localhost) \n" +
                                       "-port              - mongodb port (default 27017) \n" +
                                       "-u                 - mongodb user\n" +
                                       "-p                 - mongodb password\n" +
                                       "-source            - mongodb database with provisional data\n" +
                                       "-target            - mongodb production database to move data in\n" +
                                       "-prov              - _provenance of objects to migrate\n" +
                                       "-keep_prov         - preserve original _provenance when set\n" +
                                       "-out               - tab separated output file of migrated identifiers\n" +
                                       "-dry               - test run without actual data move\n\n" +
                                       "example: java -jar openebench-migration-tool.jar -source bench_test -tarrget openEBenchRepo -prov redmitry@list.ru";

    
    public static void main(String[] args) throws URISyntaxException {
        Map<String, List<String>> params = parameters(args);
        
        if (params.isEmpty()) {
            System.out.println(HELP);
            System.exit(0);
        }

        final List<String> ldatabase = params.get("-target");
        if (ldatabase == null || ldatabase.isEmpty()) {
            System.out.println("missed '-target' database parameter");
            System.exit(1);
        }
        
        final String target_db = ldatabase.get(0);

        final List<String> lsource = params.get("-source");
        if (lsource == null || lsource.isEmpty()) {
            System.out.println("missed '-source' database parameter");
            System.exit(1);
        }
        final String source_db = lsource.get(0);

        final List<String> lhost = params.get("-host");
        final String host = lhost == null || lhost.isEmpty() ? "localhost" : lhost.get(0);
        
        final List<String> lport = params.get("-port");
        final Integer port = lport == null || lport.isEmpty() ? 27017 : Integer.parseInt(lport.get(0));
        
        final List<String> luser = params.get("-u");
        final String user = luser == null || luser.isEmpty() ? null : luser.get(0);
        
        final List<String> lpassword = params.get("-p");
        final String password = lpassword == null || lpassword.isEmpty() ? null : lpassword.get(0);

        final List<String> lprov = params.get("-prov");
        final String prov = lprov == null || lprov.isEmpty() ? null : lprov.get(0);

        final List<String> lkeep_prov = params.get("-keep_prov");
        final boolean keep_prov = lkeep_prov != null;
        
        final List<String> ldry = params.get("-dry");
        final boolean dryrun = ldry != null;
        
        //final MongoClient client = new MongoClient(host, port);
        final URI uri = new URI("mongodb", 
                        user != null ? (user + (password != null ? ":" + password : "")) : null, host, port, null, null, null);
        
        final MongoClient client = MongoClients.create(new ConnectionString(uri.toString()));

        final MongoDatabase tgt_db = client.getDatabase(target_db);
        final MongoDatabase src_db = client.getDatabase(source_db);

        Migrator tool = new Migrator(tgt_db, src_db, new OpenEBenchSchemaResolver(), prov, keep_prov, dryrun);
            
        final String output = tool.migrate();
        
        final List<String> lout = params.get("-out");
        if (lout != null && lout.size() > 0 && output != null) {
            final String out = lout.get(0);
            try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(out), StandardOpenOption.CREATE, StandardOpenOption.WRITE)) {
                writer.write(output);
            } catch (IOException ex) {
                Logger.getLogger(MigrationTool.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private static Map<String, List<String>> parameters(String[] args) {
        TreeMap<String, List<String>> parameters = new TreeMap();        
        List<String> values = null;
        for (String arg : args) {
            switch(arg) {
                case "-host":
                case "-port":
                case "-u":
                case "-p":
                case "-source":
                case "-target":
                case "-prov":
                case "-keep_prov":
                case "-out":
                case "-dry":
                    values = parameters.get(arg);
                                      if (values == null) {
                                          values = new ArrayList(); 
                                          parameters.put(arg, values);
                                      }
                                      break;
                default: if (values != null) {
                    values.add(arg);
                }
            }
        }
        return parameters;
    }

}
