/**
 * *****************************************************************************
 * Copyright (C) 2021 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.auth;

import javax.security.enterprise.SecurityContext;
import org.bson.Document;

/**
 * @author Dmitry Repchevsky
 */

public class ContactAuthorization extends AbstractAuthorization {

    public ContactAuthorization(
            final Authorization autorization,
            final boolean privileged,
            final SecurityContext sc) {
        super(autorization, privileged, sc);
    }

    @Override
    public boolean isReadable(final Document contact) {
        
        if (privileged) {
            return isWritable(contact);
        }
        
        if (autorization.sandbox != null) {
            return false; // forbid writing communities to the sandbox
        }

        if (sc.isCallerInRole(OEBRoles.ADMIN)) {
            return true;
        }

        final String community_id = contact.getString("community_id");

        if (community_id != null && community_id.length() == 7) {
            if (sc.isCallerInRole(OEBRoles.OWNER + ":" + community_id) ||
                sc.isCallerInRole(OEBRoles.MANAGER + ":OEBE" + community_id.substring(4)) ||
                sc.isCallerInRole(OEBRoles.MANAGER + ":OEBX" + community_id.substring(4)))  {
               return true;
            }
        }

        return false;
    }
    
    @Override
    public boolean isWritable(final Document doc) {
        if (autorization.sandbox != null) {
            return false; // forbid writing communities to the sandbox
        }
        
        if (sc.isCallerInRole(OEBRoles.ADMIN)) {
            return true;
        }

        return false;        
    }
}
