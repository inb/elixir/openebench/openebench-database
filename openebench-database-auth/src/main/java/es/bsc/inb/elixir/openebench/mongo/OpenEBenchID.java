/**
 * *****************************************************************************
 * Copyright (C) 2022 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.mongo;

import org.bson.BsonNumber;
import org.bson.BsonType;
import org.bson.BsonValue;
import org.bson.Document;

/**
 * @author Dmitry Repchevsky
 */

public class OpenEBenchID {
    
    private String id;
    private Integer revision;
    
    public OpenEBenchID() {}
    
    public OpenEBenchID(String id) {
        final int idx = id.lastIndexOf('.');
        if (idx < 0) {
            this.id = id;
        } else {
            final String suffix = id.substring(idx + 1);
            try {
                this.revision = Integer.parseUnsignedInt(suffix);
                this.id = id.substring(0, idx);
            } catch (NumberFormatException ex) {
                this.id = id;
            }
        }
    }
    
    public OpenEBenchID(String id, Integer revision) {
        this.id = id;
        this.revision = revision;
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    public Integer getRevision() {
        return revision;
    }

    public void setRevision(Integer revision) {
        this.revision = revision;
    }

    @Override
    public String toString() {
        return revision == null ? id : id + "." + revision; 
    }

    public static String getBareID(String id) {
        final int idx = id.lastIndexOf('.');
        if (idx > 0) {
            try {
                Integer.parseUnsignedInt(id.substring(idx + 1));
                return id.substring(0, idx);
            } catch (NumberFormatException ex) {}            
        }
        return id;
    }

    public static String getBareID(Object pk) {
        if (pk instanceof Document) {
            return ((Document)pk).getString("id");
        }

        return pk.toString();
    }

    public static String getBareID(BsonValue pk) {
        
        final BsonType type = pk.getBsonType();
        switch(type) {
            case STRING: return pk.asString().getValue();
            case DOCUMENT:
                    return pk.asDocument().getString("id").getValue();
        }
        return null;
    }

    /**
     * Generates the temporal identifier
     * 
     * @param id
     * @param collection
     * @param community_id
     * @param username
     * 
     * @return 
     */
    public static String getTemporalOpenEBenchID(String id, String collection, 
            String community_id, String username) {
        
        if (id.matches("^OEB[A-Z]{1}[0-9]{3}t[0-9A-Z]{6}$")) {
            return id; // temporal id
        }

        final Character ch = OpenEBenchCollection.character(collection);
        if (ch == null) {
            return id; // for some collections (e.g. 'Reference') the id doesn't follow the pattern 'OEB...'
        }
        
        int hash = 7;
        for (int i = 0; i < id.length(); i++) {
            hash = hash * 31 + id.charAt(i);
        }
        for (int i = 0; i < username.length(); i++) {
            hash = hash * 31 + username.charAt(i);
        }

        final String code = Integer.toString(Math.abs(hash), 36).toUpperCase();
        final String community_code = community_id.substring(4);

        return "OEB" + ch + community_code + "t" + "000000".substring(code.length()) + code;
    }

    public static String getOpenEBenchID(final Object pk) {
        if (pk instanceof Document) {
            final String id = ((Document)pk).getString("id");
            final Integer revision = ((Document)pk).getInteger("revision");

            return revision == null ? id : id + "." + revision;   
        }

        return pk.toString();
    }
    
    public static String getOpenEBenchID(BsonValue pk) {
        
        final BsonType type = pk.getBsonType();
        switch(type) {
            case STRING: return pk.asString().getValue();
            case DOCUMENT:
                    final String id = pk.asDocument().getString("id").getValue();
                    final BsonNumber revision = pk.asDocument().getNumber("revision");
                    return revision == null ? id : id + "." + revision.intValue();
        }
        return null;
    }

    public static Integer getRevision(Object pk) {

        if (pk instanceof Document) {
            return ((Document)pk).getInteger("revision");
        }
        return null;
    }

    public static Integer getRevision(final BsonValue pk) {
        
        final BsonType type = pk.getBsonType();
        switch(type) {
            case DOCUMENT:
                    final BsonNumber revision = pk.asDocument().getNumber("revision");
                    return revision == null ? null : revision.intValue();   

        }
        return null;
    }

    public static String incOpenEBenchID(String id) {
        final char ch = id.charAt(3);
        switch(ch) {
            case 'C': {
                final String old_code = id.substring(4);
                int l = Integer.parseInt(old_code, 36);
                final String new_code = Integer.toString(l + 1, 36).toUpperCase();
                return id.substring(0, 4) + "000".substring(new_code.length()) + new_code;               
            }
            default: {
                final String old_code = id.substring(7);
                int l = Integer.parseInt(old_code, 36);
                final String new_code = Integer.toString(l + 1, 36).toUpperCase();
                return id.substring(0, 7) + "0000000".substring(new_code.length()) + new_code;
            }
        }
    }
}
