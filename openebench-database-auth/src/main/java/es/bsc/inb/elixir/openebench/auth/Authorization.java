/**
 * *****************************************************************************
 * Copyright (C) 2022 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.auth;

import com.mongodb.client.MongoDatabase;
import es.bsc.inb.elixir.openebench.mongo.OpenEBenchCollection;
import javax.security.enterprise.SecurityContext;

/**
 * Utility class to get MongoDB authentication filters.
 * 
 * @author Dmitry Repchevsky
 */

public class Authorization {

    public final MongoDatabase staged;
    public final MongoDatabase sandbox;

    public Authorization(
            final MongoDatabase staged, 
            final MongoDatabase sandbox) {
        
        this.staged = staged;
        this.sandbox = sandbox;
    }

    /**
     * Get the authorization object for a given mongodb collection.
     * 
     * @param sc security context to be used for the authorization
     * @param collection a collection for which the authorization rules should apply
     * @param privileged whether privileged access must be checked for read
     * 
     * @return the authorization object to validate documents over collection specific rules.
     *         may return null if no rules found (basically an error).
     */
    public AbstractAuthorization getAuthorization(final SecurityContext sc,
            final String collection, final boolean privileged) {
        
        switch(collection) {
            case OpenEBenchCollection.DATASET: return getDatasetAuthorization(privileged, sc);
            case OpenEBenchCollection.CHALLENGE: return getChallengeAuthorization(privileged, sc);
            case OpenEBenchCollection.CONTACT: return getContactAuthorization(privileged, sc);
            case OpenEBenchCollection.COMMUNITY: return getCommunityAuthorization(privileged, sc);
            case OpenEBenchCollection.ID_SOLV: return getIdSolvAuthorization(privileged, sc);
            case OpenEBenchCollection.PRIVILEGE: return getPrivilegeAuthorization(privileged, sc);
            case OpenEBenchCollection.REFERENCE: return getReferenceAuthorization(privileged, sc);
            case OpenEBenchCollection.TOOL: return getToolAuthorization(privileged, sc);
            case OpenEBenchCollection.TEST_ACTION: return getTestActionAuthorization(privileged, sc);
            case OpenEBenchCollection.METRICS: return getMetricsAuthorization(privileged, sc);
            case OpenEBenchCollection.BENCHMARKING_EVENT: return getBenchmarkingEventAuthorization(privileged, sc);
        }
        
        return null;
    }

    public ContactAuthorization getContactAuthorization(
            final boolean privileged, final SecurityContext sc) {
        return new ContactAuthorization(this, privileged, sc);
    }

    public CommunityAuthorization getCommunityAuthorization(
            final boolean privileged, final SecurityContext sc) {
        return new CommunityAuthorization(this, privileged, sc);
    }

    public PrivilegeAuthorization getPrivilegeAuthorization(
            final boolean privileged, final SecurityContext sc) {
        return new PrivilegeAuthorization(this, privileged, sc);
    }

    public ReferenceAuthorization getReferenceAuthorization(
            final boolean privileged, final SecurityContext sc) {
        return new ReferenceAuthorization(this, privileged, sc);
    }

    public ChallengeAuthorization getChallengeAuthorization(
            final boolean privileged, final SecurityContext sc) {
        return new ChallengeAuthorization(this, privileged, sc);
    }

    public DatasetAuthorization getDatasetAuthorization(
            final boolean privileged, final SecurityContext sc) {
        return new DatasetAuthorization(this, privileged, sc);
    }

    public IdSolvAuthorization getIdSolvAuthorization(
            final boolean privileged, final SecurityContext sc) {
        return new IdSolvAuthorization(this, privileged, sc);
    }
    
    public ToolAuthorization getToolAuthorization(
            final boolean privileged, final SecurityContext sc) {
        return new ToolAuthorization(this, privileged, sc);
    }
    
    public TestActionAuthorization getTestActionAuthorization(
            final boolean privileged, final SecurityContext sc) {
        return new TestActionAuthorization(this, privileged, sc);
    }

    public MetricsAuthorization getMetricsAuthorization(
            final boolean privileged, final SecurityContext sc) {
        return new MetricsAuthorization(this, privileged, sc);
    }
    
    public BenchmarkingEventAuthorization getBenchmarkingEventAuthorization(
            final boolean privileged, final SecurityContext sc) {
        return new BenchmarkingEventAuthorization(this, privileged, sc);
    }

}
