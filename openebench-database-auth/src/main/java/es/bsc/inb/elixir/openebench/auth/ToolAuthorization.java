/**
 * *****************************************************************************
 * Copyright (C) 2020 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.auth;

import java.util.List;
import javax.security.enterprise.SecurityContext;
import org.bson.Document;

/**
 * @author Dmitry Repchevsky
 */

public class ToolAuthorization extends AbstractAuthorization {

    public ToolAuthorization(
            final Authorization autorization,
            final boolean privileged,
            final SecurityContext sc) {

        super(autorization, privileged, sc);
    }

    @Override
    public boolean isReadable(final Document tool) {
        
        if (privileged) {
            return isWritable(tool);
        }
        
        if (sc.isCallerInRole(OEBRoles.ADMIN)) {
            return true;
        }

        final List<String> community_ids = tool.get("community_ids", List.class);
        for (String community_id : community_ids) {
            if (sc.isCallerInRole(OEBRoles.OWNER + ":" + community_id)) {
                return true;
            }
        }
        return true;
    }
    
    @Override
    public boolean isWritable(final Document tool) {
        if (sc.isCallerInRole(OEBRoles.ADMIN)) {
            return true;
        }

        final List<String> community_ids = tool.get("community_ids", List.class);
        for (String community_id : community_ids) {
            if (sc.isCallerInRole(OEBRoles.OWNER + ":" + community_id)) {
                return true;
            }
        }
        return false;        
    }
}
