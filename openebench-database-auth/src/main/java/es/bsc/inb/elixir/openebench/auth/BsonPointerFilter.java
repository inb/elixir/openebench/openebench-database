/**
 * *****************************************************************************
 * Copyright (C) 2022 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 **/

package es.bsc.inb.elixir.openebench.auth;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import org.bson.BsonArray;
import org.bson.BsonDocument;
import org.bson.BsonValue;
import org.bson.Document;

/**
 * @author Dmitry Repchevsky
 */

public final class BsonPointerFilter {

    public static void filter(final Document doc, final List<String> pointers) {
        
        final TreeSet<String> set = new TreeSet(pointers);
        if (set.contains("/")) {
            final Iterator iter = doc.entrySet().iterator();
            while (iter.hasNext()) {
                iter.remove();
            }
        } else {
            filter(doc, set, "/");
        }
    }

    public static void filter(final BsonDocument doc, final List<String> pointers) {
        
        final TreeSet<String> set = new TreeSet(pointers);
        if (set.contains("/")) {
            final Iterator iter = doc.entrySet().iterator();
            while (iter.hasNext()) {
                iter.remove();
            }
        } else {
            filter(doc, set, "/");
        }
    }

    private static void filter(final Document doc, 
            final Set<String> pointers, final String pointer) {

        final Iterator<Map.Entry<String, Object>> iter = doc.entrySet().iterator();
        while (iter.hasNext()) {
            final Map.Entry<String, Object> entry = iter.next();
            final String name = entry.getKey();
            final String next = pointer + name;
            if (pointers.contains(next)) {
                iter.remove();
            } else {
                final Object value = entry.getValue();
                if (value instanceof Document) {
                    filter((Document)value, pointers, next + "/");
                } else if (value instanceof List) {
                    filter((List)value, pointers, next + "/");
                }
            }
        }
    }
    
    private static void filter(final BsonDocument doc, 
            final Set<String> pointers, final String pointer) {

        final Iterator<Map.Entry<String, BsonValue>> iter = doc.entrySet().iterator();
        while (iter.hasNext()) {
            final Map.Entry<String, BsonValue> entry = iter.next();
            final String name = entry.getKey();
            final String next = pointer + name;
            if (pointers.contains(next)) {
                iter.remove();
            } else {
                final BsonValue value = entry.getValue();
                switch (value.getBsonType()) {
                    case DOCUMENT: filter(value.asDocument(), pointers, next + "/"); break;
                    case ARRAY: filter(value.asArray(), pointers, next + "/"); break;
                }
            }
        }
    }

    private static void filter(final List<Document> list, 
            final Set<String> pointers, final String pointer) {

        for (Object value : list) {
            if (value instanceof Document) {
                filter((Document)value, pointers, pointer); break;
            } else if (value instanceof List) {
                filter((List)value, pointers, pointer); break;
            }
        }
    }

    private static void filter(final BsonArray array, 
            final Set<String> pointers, final String pointer) {

        for (BsonValue value : array.getValues()) {
            switch (value.getBsonType()) {
                case DOCUMENT: filter(value.asDocument(), pointers, pointer); break;
                case ARRAY: filter(value.asArray(), pointers, pointer); break;
            }
        }
    }
}
