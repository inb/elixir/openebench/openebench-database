/**
 * *****************************************************************************
 * Copyright (C) 2023 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.auth;

import javax.security.enterprise.SecurityContext;
import es.bsc.inb.elixir.openebench.mongo.OpenEBenchID;
import org.bson.Document;

/**
 * @author Dmitry Repchevsky
 */

public class BenchmarkingEventAuthorization extends AbstractAuthorization {
    
    public BenchmarkingEventAuthorization(
            final Authorization autorization,
            final boolean privileged,
            final SecurityContext sc) {
        super(autorization, privileged, sc);
    }

    @Override
    public boolean isReadable(final Document benchmarking_event) {
        return privileged ? isWritable(benchmarking_event) : true;
    }
    
    @Override
    public boolean isWritable(final Document benchmarking_event) {
        if (sc.isCallerInRole(OEBRoles.ADMIN)) {
            return true;
        }

        final Object benchmarking_pk = benchmarking_event.get("_id");
        final String benchmarking_bare_id = OpenEBenchID.getBareID(benchmarking_pk);
        if (sc.isCallerInRole(OEBRoles.MANAGER + ":" + benchmarking_bare_id)) {
            return true;
        }
        
        final String community_id = benchmarking_event.getString("community_id");
        if (community_id != null) {
            final String community_bare_id = OpenEBenchID.getBareID(community_id);
            if (sc.isCallerInRole(OEBRoles.OWNER + ":" + community_bare_id)) {
                return true;
            }
        }
        
        return false;        
    }
}
