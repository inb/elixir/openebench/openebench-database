/**
 * *****************************************************************************
 * Copyright (C) 2022 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.auth;

import com.mongodb.client.MongoCollection;
import es.bsc.inb.elixir.openebench.mongo.MongoQueries;
import es.bsc.inb.elixir.openebench.mongo.OpenEBenchCollection;
import es.bsc.inb.elixir.openebench.mongo.OpenEBenchID;
import java.util.HashMap;
import java.util.Map;
import javax.security.enterprise.SecurityContext;
import org.bson.Document;

/**
 * @author Dmitry Repchevsky
 */

public class TestActionAuthorization extends AbstractAuthorization {

    private final Map<String, Document> communities_map;
    
    private final MongoCollection<Document> staged_events;
    private final MongoCollection<Document> sandbox_events;

    private final MongoCollection<Document> staged_challenges;
    private final MongoCollection<Document> sandbox_challenges;

    public TestActionAuthorization(
            final Authorization autorization,
            final boolean privileged,
            final SecurityContext sc) {
        
        super(autorization, privileged, sc);

        communities_map = new HashMap();

        staged_events = autorization.staged.getCollection(OpenEBenchCollection.BENCHMARKING_EVENT); 
        sandbox_events = autorization.sandbox != null ? autorization.sandbox.getCollection(OpenEBenchCollection.BENCHMARKING_EVENT) : null;

        staged_challenges = autorization.staged.getCollection(OpenEBenchCollection.CHALLENGE); 
        sandbox_challenges = autorization.sandbox != null ? autorization.sandbox.getCollection(OpenEBenchCollection.CHALLENGE) : null;
    }

    @Override
    public boolean isReadable(final Document test_action) {
        return isWritable(test_action);
    }
    
    @Override
    public boolean isWritable(final Document test_action) {
        if (sc.isCallerInRole(OEBRoles.ADMIN)) {
            return true;
        }

        final String challenge_id = test_action.getString("challenge_id");
        if (challenge_id != null) {
            final String challenge_bare_id = OpenEBenchID.getBareID(challenge_id);
            
            // contributor may write only to provisional database.
            if (sandbox_challenges != null &&
                sc.isCallerInRole(OEBRoles.CONTRIBUTOR + ":" + challenge_bare_id)) {
                return true;
            }

            String community_id;
            Document event = communities_map.get(challenge_id);
            if (event != null) {
                final String benchmarking_event_bare_id = OpenEBenchID.getBareID(event.get("_id"));
                if (sc.isCallerInRole(OEBRoles.MANAGER + ":" + benchmarking_event_bare_id) ||
                    (sandbox_challenges != null && 
                     sc.isCallerInRole(OEBRoles.CONTRIBUTOR + ":" + benchmarking_event_bare_id))) {
                    return true;
                }  
                community_id = event.getString("community_id");
            } else {
                Document challenge = MongoQueries.findDocument(staged_challenges, challenge_id).first();
                if (challenge == null &&
                   (sandbox_challenges == null ||
                   (challenge = MongoQueries.findDocument(sandbox_challenges, challenge_id).first()) == null)) {
                    return false;
                }
                
                final String benchmarking_event_id = challenge.getString("benchmarking_event_id");
                if (benchmarking_event_id == null) {
                    return false;
                }

                final String benchmarking_event_bare_id = OpenEBenchID.getBareID(benchmarking_event_id);
                if (sc.isCallerInRole(OEBRoles.MANAGER + ":" + benchmarking_event_bare_id) ||
                    (sandbox_challenges != null && 
                     sc.isCallerInRole(OEBRoles.CONTRIBUTOR + ":" + benchmarking_event_bare_id))) {
                    return true;
                }

                event = MongoQueries.findDocument(staged_events, benchmarking_event_id).first();
                if (event == null &&
                   (sandbox_events == null ||
                   (event = MongoQueries.findDocument(sandbox_events, benchmarking_event_id).first()) == null)) {
                    return false;
                }
                
                community_id = event.getString("community_id");
                if (community_id == null) {
                    return false;
                }

                communities_map.put(challenge_id, event);
            }

            if (sc.isCallerInRole(OEBRoles.OWNER + ":" + community_id)) {
                return true;
            }

        }
        
        return false;        
    }
}
