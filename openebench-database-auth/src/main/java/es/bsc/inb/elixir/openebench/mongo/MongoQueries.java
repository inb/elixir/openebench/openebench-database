/**
 * *****************************************************************************
 * Copyright (C) 2021 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 * *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.mongo;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bson.BsonDocument;
import org.bson.BsonString;
import org.bson.BsonValue;
import org.bson.Document;
import org.bson.conversions.Bson;

/**
 * @author Dmitry Repchevsky
 */

public class MongoQueries {

    /**
     * Find a document by its OpenEBench identifier. The identifier can be in 
     * either short or canonical form (i.e. 'OEBX0000000000' or 'OEBX0000000000.1').
     * 
     * @param documents
     * @param id
     * @return 
     */
    public static <T> FindIterable<T> findDocument(
            final MongoCollection<T> documents, final String id) {
        
        switch(documents.getNamespace().getCollectionName()) {
            case OpenEBenchCollection.CONTACT:
            case OpenEBenchCollection.PRIVILEGE:
            case OpenEBenchCollection.COMMUNITY:
            case OpenEBenchCollection.REFERENCE: return documents.find(Filters.eq("_id", id));
        }
    
        // we presume that temporal ids are for the sandbox only 
        if (id.matches("^OEB[A-Z]{1}[0-9]{3}t[0-9A-Z]{6}$")) {
            return documents.find(Filters.eq("_id", id));
        }

        final int idx = id.lastIndexOf('.');
        if (idx < 0) {
            // return the last revision
            return documents.find(Filters.eq("_id.id", id)).sort(new Document("_id.revision", -1)).limit(1);
        } else {
            final String suffix = id.substring(idx + 1);
            try {
                final int revision = Integer.parseUnsignedInt(suffix);
                return documents.find(Filters.and(
                        Filters.eq("_id.id", id.substring(0, idx)),
                        Filters.eq("_id.revision", revision)));
            } catch (NumberFormatException ex) {
                Logger.getLogger(MongoQueries.class.getName()).log(Level.WARNING, "invalid revision: {0}", id);
            }
        }

        return documents.find(Filters.eq("_id", id));
    }
    
    public static boolean hasDocument(MongoCollection documents, String id) {
        
        switch(documents.getNamespace().getCollectionName()) {
            case OpenEBenchCollection.CONTACT:
            case OpenEBenchCollection.PRIVILEGE:
            case OpenEBenchCollection.COMMUNITY:
            case OpenEBenchCollection.REFERENCE: return documents.countDocuments(Filters.eq("_id", id)) > 0;
        }

        return documents.countDocuments(getOpenEBenchIdFilter(id)) > 0;
    }

//    public static FindIterable<Document> find(MongoCollection<Document> col, 
//            JsonObject filter) {
//
//        if (filter == null || filter.isEmpty()) {
//            return col.find();
//        }
//
//        final List<Bson> filters = new ArrayList();
//        setFilters(filters, null, filter);
//                
//        return filters.isEmpty() ? col.find() : col.find(Filters.and(filters));
//    }

    public static FindIterable<Document> find(
            final MongoCollection<Document> col, final BsonDocument filter) {
        if (filter == null || filter.isEmpty()) {
            return col.find();
        }

        List<Bson> filters = new ArrayList();
        for (Map.Entry<String, BsonValue> entry : filter.entrySet()) {
            final String property = entry.getKey();
            final BsonValue value = entry.getValue();
            switch (value.getBsonType()) {
                case STRING: if ("_id".equals(property)) {
                                 filters.add(MongoQueries.getOpenEBenchIdFilter(value.asString().getValue()));
                             } else {
                                 filters.add(Filters.eq(property, value.asString().getValue()));
                             }
                              break;
                case INT32: filters.add(Filters.eq(property, value.asInt32())); break;
                case INT64: filters.add(Filters.eq(property, value.asInt64())); break;
                case DOUBLE: filters.add(Filters.eq(property, value.asDouble())); break;
                case DECIMAL128: filters.add(Filters.eq(property, value.asDecimal128())); break;
                case BOOLEAN: filters.add(Filters.eq(property, value.asBoolean())); break;
                case NULL: filters.add(Filters.eq(property, null)); break;
                case DOCUMENT: if ("_id".equals(property)) {
                                final String id = value.asDocument().getString("id").getValue();
                                final BsonString rev = value.asDocument().getString("revision", null);
                                filters.add(MongoQueries.getOpenEBenchIdFilter(rev == null ? id : id + "." + rev.getValue())); break;
                }
            }
        }
        
        return filters.isEmpty() ? col.find() : col.find(Filters.and(filters));
    }

    /**
     * Construct BSON object for the MongoDB search by ID query.
     * 
     * @param id OpenEBench identifier (OEB...) or any (e.g. DOI).
     * 
     * @return appropriate object to search MongoDB (e.g. '{ _id.id: xxx, _id.revision: yyy }')
     */
    public static Bson getOpenEBenchIdFilter(final String id) {
        return getOpenEBenchIdFilter(id, false);
    }

    public static Bson getOpenEBenchIdFilter(final String id, final boolean loozy) {
        // we presume that temporal ids are for the sandbox only 
        if (id.matches("^OEB[A-Z]{1}[0-9]{3}t[0-9A-Z]{6}$")) {
            return Filters.eq("_id", id);
        }

        if (id.matches("^OEB[A-Z]{1}[0-9]{3}[0-9A-Z]{7}$")) {
            final int idx = id.lastIndexOf('.');
            if (idx < 0) {
                return Filters.eq("_id.id", id);
            } else {
                final String suffix = id.substring(idx + 1);
                try {
                    final int revision = Integer.parseUnsignedInt(suffix);
                    return Filters.and(
                            Filters.eq("_id.id", id.substring(0, idx)),
                            Filters.eq("_id.revision", revision));
                } catch (NumberFormatException ex) {
                    Logger.getLogger(MongoQueries.class.getName()).log(Level.WARNING, "invalid revision: {0}", id);
                }
            }
        }
        
        return loozy ? Filters.expr(new BsonDocument("$regexMatch", 
                new BsonDocument("input", new BsonString(id)).append("regex", new BsonString("$$CURRENT._id"))))
                : Filters.eq("_id", id);
    }
}
