/**
 * *****************************************************************************
 * Copyright (C) 2022 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.auth;

import com.mongodb.client.MongoCollection;
import es.bsc.inb.elixir.openebench.mongo.MongoQueries;
import es.bsc.inb.elixir.openebench.mongo.OpenEBenchCollection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.security.enterprise.SecurityContext;
import org.bson.Document;

/**
 * @author Dmitry Repchevsky
 */

public class PrivilegeAuthorization extends AbstractAuthorization {

    private final Map<String, Document> benchmarking_events_map;
    private final Map<String, Document> challenges_map;

    private final MongoCollection<Document> benchmarking_events;
    private final MongoCollection<Document> challenges;

    public PrivilegeAuthorization(
            final Authorization autorization, 
            final boolean privileged,
            final SecurityContext sc) {
        
        super(autorization, privileged, sc);

        benchmarking_events_map = new HashMap();
        challenges_map = new HashMap();
        
        benchmarking_events = autorization.staged.getCollection(OpenEBenchCollection.BENCHMARKING_EVENT); 
        challenges = autorization.staged.getCollection(OpenEBenchCollection.CHALLENGE); 
    }

    @Override
    public boolean isReadable(final Document privilege) {
        
        if (privileged) {
            return isWritable(privilege);
        }
        
        if (autorization.sandbox != null) {
            return false; // forbid writing communities to the sandbox
        }

        if (sc.isCallerInRole(OEBRoles.ADMIN)) {
            return true;
        }

        final List<Document> roles = privilege.get("roles", List.class);
        for (Iterator<Document> iter = roles.iterator();iter.hasNext();) {
            if (!isRoleReadable(iter.next())) {
                iter.remove();
            }
        }
        return !roles.isEmpty();
    }
    
    @Override
    public boolean isWritable(final Document doc) {
        if (autorization.sandbox != null) {
            return false; // forbid writing communities to the sandbox
        }

        if (sc.isCallerInRole(OEBRoles.ADMIN)) {
            return true;
        }

        return false;        
    }
    
    private boolean isRoleReadable(final Document role) {
        final String role_name = role.getString("role");

        switch(role_name) {
            case OEBRoles.CONTRIBUTOR: {
                final String challenge_id = role.getString("challenge_id");
                if (challenge_id != null && 
                    sc.isCallerInRole(OEBRoles.CONTRIBUTOR + ":" + challenge_id)) {
                    return true; // supervisors can see participants of their challenge
                }
                final String benchmarking_event_id = getBenchmarkingEventId(challenge_id);
                if (benchmarking_event_id != null && 
                    (sc.isCallerInRole(OEBRoles.MANAGER + ":" + benchmarking_event_id) ||
                     sc.isCallerInRole(OEBRoles.CONTRIBUTOR + ":" + benchmarking_event_id))) {
                    return true; // managers can see all participants of the event
                }
                final String community_id = getCommunityId(benchmarking_event_id);
                if (community_id != null && 
                    sc.isCallerInRole(OEBRoles.OWNER + ":" + community_id)) {
                    return true; // owners can see all participants of the community
                }                
            }
                
        }
        return false;
    }
    
    private String getBenchmarkingEventId(final String challenge_id) {
        if (challenge_id == null) {
            return null;
        }
        
        Document challenge = challenges_map.get(challenge_id);
        if (challenge == null) {
            challenge = MongoQueries.findDocument(challenges, challenge_id).first();
            if (challenge == null) {
                return null;
            }
            challenges_map.put(challenge_id, challenge);
        }
        return challenge.getString("benchmarking_event_id");
    }
    
    private String getCommunityId(final String benchmarking_event_id) {
        if (benchmarking_event_id == null) {
            return null;
        }

        Document benchmarking_event = benchmarking_events_map.get(benchmarking_event_id);
        if (benchmarking_event == null) {
            benchmarking_event = MongoQueries.findDocument(benchmarking_events, benchmarking_event_id).first();
            if (benchmarking_event == null) {
                return null;
            }
            benchmarking_events_map.put(benchmarking_event_id, benchmarking_event);
        }
        return benchmarking_event.getString("community_id");
    }
}
