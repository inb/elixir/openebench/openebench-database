/**
 * *****************************************************************************
 * Copyright (C) 2020 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.auth;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCursor;
import java.io.Closeable;
import java.util.Iterator;
import org.bson.Document;

/**
 * Proxy MongoDB iterator filter that filers out all documents that doesn't 
 * pass authorization.checkAccess() verification.
 * 
 * @author Dmitry Repchevsky
 */

public class CollectionAuthorizationFilter 
        implements Iterator<Document>, Closeable, AutoCloseable {

    private final AbstractAuthorization authz;
    
    private final MongoCursor<Document> cursor;
    private Document next;
    
    public CollectionAuthorizationFilter(
            final AbstractAuthorization authz,
            final FindIterable<Document> iter) {

        this.authz = authz;
        cursor = iter.iterator();
    }

    public boolean isReadable(final Document doc) {
        return authz == null || authz.isReadable(doc);
    }

    @Override
    public boolean hasNext() {
        while(next == null && cursor.hasNext()) {
            if (isReadable(next = cursor.next())) {
                break;
            }
            next = null;
        }
        return next != null;
    }

    @Override
    public Document next() {
        hasNext();

        final Document doc = next;
        next = null;
        return doc;
    }
    
    @Override
    public void close() {
        cursor.close();
    }
    
}
