/**
 * *****************************************************************************
 * Copyright (C) 2022 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.auth;

import es.bsc.inb.elixir.openebench.mongo.OpenEBenchMeta;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.security.enterprise.SecurityContext;
import org.bson.Document;

/**
 * @author Dmitry Repchevsky
 */

public abstract class AbstractAuthorization {
    
    protected final Authorization autorization;
    protected final boolean privileged;
    protected final SecurityContext sc;
    
    public AbstractAuthorization(
            final Authorization autorization,
            final boolean privileged,
            final SecurityContext sc) {

        this.autorization = autorization;
        this.privileged = privileged;
        this.sc = sc;
    }
    
    public abstract boolean isReadable(Document doc);
    public abstract boolean isWritable(Document doc);
    
    final protected void filter(final Document doc) {
        try {
            final Object obj = doc.get(OpenEBenchMeta.METADATA_PROPERTY);
            if (obj instanceof Document) {
                final Document meta = (Document)obj;
                final List<String> prot = meta.getList(OpenEBenchMeta.PROTECTED_PROPERTIES, String.class);
                if (prot != null && !prot.isEmpty()) {
                    BsonPointerFilter.filter(doc, prot);
                }
            }
        } catch (Exception ex) {
             Logger.getLogger(AbstractAuthorization.class.getName()).log(Level.WARNING, ex.getMessage());
        }
    }
}
