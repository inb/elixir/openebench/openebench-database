/**
 * *****************************************************************************
 * Copyright (C) 2022 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.auth;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import es.bsc.inb.elixir.openebench.mongo.OpenEBenchCollection;
import es.bsc.inb.elixir.openebench.mongo.OpenEBenchID;
import javax.security.enterprise.SecurityContext;
import org.bson.Document;

/**
 * @author Dmitry Repchevsky
 */

public class MetricsAuthorization extends AbstractAuthorization {

    private final MongoCollection<Document> communities;
    
    public MetricsAuthorization(
            final Authorization autorization, 
            final boolean privileged,
            final SecurityContext sc) {
        
        super(autorization, privileged, sc);
        
        communities = autorization.staged.getCollection(OpenEBenchCollection.COMMUNITY); 
    }

    @Override
    public boolean isReadable(final Document metrics) {
        
        if (privileged) {
            return isWritable(metrics);
        }
        
        if (sc.isCallerInRole(OEBRoles.ADMIN)) {
            return true;
        }

        return true;
    }
    
    @Override
    public boolean isWritable(final Document metrics) {
        if (sc.isCallerInRole(OEBRoles.ADMIN)) {
            return true;
        }

        final FindIterable<Document> iter = communities.find().projection(new Document("_id", 1));
        try (MongoCursor<Document> cursor = iter.cursor()) {
            while (cursor.hasNext()) {
                final Document community = cursor.next();
                final Object community_pk = community.get("_id");
                final String community_bare_id = OpenEBenchID.getBareID(community_pk);
                if (sc.isCallerInRole(OEBRoles.OWNER + ":" + community_bare_id)) {
                    return true;
                }
            }
        }
        
        return false;        
    }
}
