/**
 * *****************************************************************************
 * Copyright (C) 2023 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.auth;

import com.mongodb.client.MongoCollection;
import es.bsc.inb.elixir.openebench.mongo.MongoQueries;
import es.bsc.inb.elixir.openebench.mongo.OpenEBenchCollection;
import es.bsc.inb.elixir.openebench.mongo.OpenEBenchID;
import java.util.HashMap;
import java.util.Map;
import javax.security.enterprise.SecurityContext;
import org.bson.Document;

/**
 * @author Dmitry Repchevsky
 */

public class ChallengeAuthorization extends AbstractAuthorization {
    
    private final Map<String, String> communities_map;
    
    private final MongoCollection<Document> staged_events;
    private final MongoCollection<Document> sandbox_events;

    public ChallengeAuthorization(
            final Authorization autorization,
            final boolean privileged,
            final SecurityContext sc) {
        super(autorization, privileged, sc);
        
        communities_map = new HashMap();

        staged_events = autorization.staged.getCollection(OpenEBenchCollection.BENCHMARKING_EVENT); 
        sandbox_events = autorization.sandbox != null ? 
                autorization.sandbox.getCollection(OpenEBenchCollection.BENCHMARKING_EVENT) : null;
    }
    
    @Override
    public boolean isReadable(final Document challenge) {
        return privileged ? isWritable(challenge) : true;
    }
    
    @Override
    public boolean isWritable(final Document challenge) {
        if (sc.isCallerInRole(OEBRoles.ADMIN)) {
            return true;
        }
        
        final Object challenge_pk = challenge.get("_id");
        final String challenge_bare_id = OpenEBenchID.getBareID(challenge_pk);
        
        if (sc.isCallerInRole(OEBRoles.MANAGER + ":" + challenge_bare_id)) {
            return true;
        }
        
        String community_id = communities_map.get(challenge_bare_id);
        if (community_id == null) {
            final String benchmarking_event_id = challenge.getString("benchmarking_event_id");
            if (benchmarking_event_id == null) {
                return false;
            }

            final String benchmarking_event_bare_id = OpenEBenchID.getBareID(benchmarking_event_id);
            if (sc.isCallerInRole(OEBRoles.MANAGER + ":" + OpenEBenchID.getBareID(benchmarking_event_bare_id))) {
                return true;
            }
        
            Document event = MongoQueries.findDocument(staged_events, benchmarking_event_id).first();
            if (event == null &&
               (sandbox_events == null ||
               (event = MongoQueries.findDocument(sandbox_events, benchmarking_event_id).first()) == null)) {
                return false;
            }

            community_id = event.getString("community_id");
            if (community_id == null) {
                return false;
            }
            communities_map.put(challenge_bare_id, community_id);
        }
        
        return sc.isCallerInRole(OEBRoles.OWNER + ":" + community_id);
    }
}
