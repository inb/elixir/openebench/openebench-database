/**
 * *****************************************************************************
 * Copyright (C) 2022 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 * *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.mongo;

/**
 * OpenEBench MongoDB collection constant names.
 * 
 * @author Dmitry Repchevsky
 */

public final class OpenEBenchCollection {
    
    public final static String CONTACT = "Contact";
    public final static String PRIVILEGE = "Privilege";
    public final static String DATASET = "Dataset";
    public final static String CHALLENGE = "Challenge";
    public final static String COMMUNITY = "Community";
    public final static String BENCHMARKING_EVENT = "BenchmarkingEvent";
    public final static String TOOL = "Tool";
    public final static String TEST_ACTION = "TestAction";
    public final static String ID_SOLV = "IdSolv";
    public final static String METRICS = "Metrics";
    public final static String METRICS_CATEGORY = "MetricsCategory";
    public final static String REFERENCE = "Reference";
    
    public final static boolean isOpenEBenchCollection(final String collection) {
        switch(collection) {
            case CONTACT:
            case PRIVILEGE:
            case DATASET:
            case CHALLENGE:
            case COMMUNITY:
            case BENCHMARKING_EVENT:
            case TOOL:
            case TEST_ACTION:
            case ID_SOLV:
            case METRICS:
            case METRICS_CATEGORY:
            case REFERENCE: return true;
        }
        return false;
    }

    /**
     * Returns mongodb collection form the identifier.
     * 
     * @param id
     * 
     * @return mongodb collection or 'null' if identifier is incorrect.
     */
    public static String collection(final String id) {
        if (id.length() > 3) {
            final char ch = id.charAt(3);
            switch(ch) {
                case 'D': return OpenEBenchCollection.DATASET;
                case 'E': return OpenEBenchCollection.BENCHMARKING_EVENT;
                case 'X': return OpenEBenchCollection.CHALLENGE;
                case 'C': return OpenEBenchCollection.COMMUNITY;
                case 'M': return OpenEBenchCollection.METRICS;
                case 'Y': return OpenEBenchCollection.METRICS_CATEGORY;
                case 'A': return OpenEBenchCollection.TEST_ACTION;
                case 'T': return OpenEBenchCollection.TOOL;
            }
        }
        return null;        
    }
    
    /**
     * Returns the character associated to the collection (e.g. 'D' for 'Dataset')
     * 
     * @param collection
     * 
     * @return character or null if no association (e.g. 'Reference')
     */
    public static Character character(final String collection) {
        switch(collection) {
            case OpenEBenchCollection.DATASET:            return 'D';
            case OpenEBenchCollection.BENCHMARKING_EVENT: return 'E';
            case OpenEBenchCollection.CHALLENGE:          return 'X';
            case OpenEBenchCollection.METRICS:            return 'M';
            case OpenEBenchCollection.METRICS_CATEGORY:   return 'Y';
            case OpenEBenchCollection.TEST_ACTION:        return 'A';
            case OpenEBenchCollection.TOOL:               return 'T';
            case OpenEBenchCollection.COMMUNITY:          return 'C';
        }
        return null;        
    }
}
