/**
 * *****************************************************************************
 * Copyright (C) 2022 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 * *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.mongo;

import es.bsc.inb.elixir.openebench.auth.BsonPointerFilter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bson.BsonArray;
import org.bson.BsonDocument;
import org.bson.BsonValue;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Dmitry Repchevsky
 */

public class BsonPointerFilterTest {

    public static final String COMMUNITY_FILE = "json/community.json";
    
    @Test
    public void test() {

        try {
            final String community_file = Files.readString(
                Paths.get(BsonPointerFilterTest.class.getClassLoader().getResource(COMMUNITY_FILE).toURI())); 

            final BsonDocument community = BsonDocument.parse(community_file);
            
            BsonPointerFilter.filter(community, Arrays.asList("/@provenance", "/links/label"));
        
            Assert.assertNull(community.get("@provenance"));

            final BsonArray links = community.getArray("links");
            for (BsonValue link : links.getValues()) {
                Assert.assertNull(link.asDocument().get("label"));
            }
            
        } catch (IOException | URISyntaxException ex) {
            Logger.getLogger(BsonPointerFilterTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail(ex.getMessage());
        }
    }
}
