/**
 * *****************************************************************************
 * Copyright (C) 2019 Spanish National Bioinformatics Institute (INB) and
 * Barcelona Supercomputing Center
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.mongo.validator;

import com.mongodb.ConnectionString;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;
import es.bsc.inb.elixir.openebench.validator.FKValidator;
import es.elixir.bsc.json.schema.ValidationError;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author Dmitry Repchevsky
 */

public class ValidatorTool {
    
    private final static String HELP = "validator [-host, -port, -u, -p] -staged [-sandbox]\n\n" +
                                       "parameters:\n\n" +
                                       "-host              - mongodb host (default localhost) \n" +
                                       "-port              - mongodb port (default 27017) \n" +
                                       "-u                 - mongodb user\n" +
                                       "-p                 - mongodb password\n" +
                                       "-staged            - mongodb database to validate\n" +
                                       "-sandbox           - mongodb additional (sandbox) database to validate\n";
    
   public static void main(String[] args) throws URISyntaxException {
        Map<String, List<String>> params = parameters(args);
        
        if (params.isEmpty()) {
            System.out.println(HELP);
            System.exit(0);
        }

        final List<String> lstaged = params.get("-staged");
        if (lstaged.isEmpty()) {
            System.out.println("missed 'database' parameter");
            System.exit(1);
        }
        final String staged = lstaged.get(0);

        final List<String> lhost = params.get("-host");
        final String host = lhost == null || lhost.isEmpty() ? "localhost" : lhost.get(0);
        
        final List<String> lport = params.get("-port");
        final Integer port = lport == null || lport.isEmpty() ? 27017 : Integer.parseInt(lport.get(0));
        
        final List<String> luser = params.get("-u");
        final String user = luser == null || luser.isEmpty() ? null : luser.get(0);
        
        final List<String> lpassword = params.get("-p");
        final String password = lpassword == null || lpassword.isEmpty() ? null : lpassword.get(0);

        final List<String> lsandbox = params.get("-sandbox");
        final String sandbox = lsandbox == null || lsandbox.isEmpty() ? staged : lsandbox.get(0);

        final URI uri = new URI("mongodb", 
                        user != null ? (user + (password != null ? ":" + password : "")) : null, host, port, null, null, null);
        
        final MongoClient client = MongoClients.create(new ConnectionString(uri.toString()));

        final MongoDatabase staged_db = client.getDatabase(staged);
        
        final FKValidator validator = new FKValidator(staged_db);
        
        final List<ValidationError> errors = new ArrayList<>();
        
        validator.validate(staged_db, errors);
        
        if (sandbox != null) {
            final MongoDatabase sandbox_db = client.getDatabase(sandbox);
            validator.validate(sandbox_db, errors);
        }
        
        for (ValidationError error : errors) {
            System.out.println(error.message);
        }
    }
    
    private static Map<String, List<String>> parameters(String[] args) {
        TreeMap<String, List<String>> parameters = new TreeMap();        
        List<String> values = null;
        for (String arg : args) {
            switch(arg) {
                case "-host":
                case "-port":
                case "-u":
                case "-p":
                case "-staged":
                case "-sandbox":  values = parameters.get(arg);
                                  if (values == null) {
                                      values = new ArrayList(); 
                                      parameters.put(arg, values);
                                  }
                                  break;
                default: if (values != null) {
                    values.add(arg);
                }
            }
        }
        return parameters;
    }
}
