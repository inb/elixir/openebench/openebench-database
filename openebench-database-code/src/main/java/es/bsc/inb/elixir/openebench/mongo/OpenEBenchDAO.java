/**
 * *****************************************************************************
 * Copyright (C) 2023 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 * *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.mongo;

import com.mongodb.BasicDBObject;
import com.mongodb.ErrorCategory;
import com.mongodb.MongoWriteException;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoIterable;
import com.mongodb.client.gridfs.GridFSBucket;
import com.mongodb.client.gridfs.GridFSBuckets;
import com.mongodb.client.gridfs.GridFSFindIterable;
import com.mongodb.client.gridfs.model.GridFSFile;
import com.mongodb.client.gridfs.model.GridFSUploadOptions;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.FindOneAndUpdateOptions;
import com.mongodb.client.model.Projections;
import com.mongodb.client.model.ReturnDocument;
import com.mongodb.client.model.Updates;
import com.mongodb.client.result.DeleteResult;
import es.bsc.inb.elixir.openebench.auth.AbstractAuthorization;
import es.bsc.inb.elixir.openebench.auth.Authorization;
import es.bsc.inb.elixir.openebench.auth.CollectionAuthorizationFilter;
import es.bsc.inb.elixir.openebench.auth.OEBRoles;
import es.bsc.inb.elixir.openebench.auth.PayloadAuthorization;
import es.bsc.inb.elixir.openebench.validator.AbstractOEBSchemaResolver;
import es.bsc.inb.elixir.openebench.validator.DependencyValidationError;
import static es.bsc.inb.elixir.openebench.validator.DependencyValidationError.MESSAGE;
import es.bsc.inb.elixir.openebench.validator.JsonObjectValidator;
import es.bsc.inb.elixir.openebench.validator.ObjectDependencyValidator;
import es.bsc.inb.elixir.openebench.validator.ValidationException;
import es.bsc.inb.elixir.openebench.validator.PKValidationError;
import es.elixir.bsc.json.schema.JsonSchemaLocator;
import es.elixir.bsc.json.schema.ValidationError;
import es.bsc.inb.elixir.openebench.validator.MongoFilteredCollectionFactory;
import java.io.InputStream;
import java.io.StringReader;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.Principal;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.json.Json;
import javax.json.JsonException;
import javax.json.JsonMergePatch;
import javax.json.JsonObject;
import javax.json.JsonPatch;
import javax.json.JsonPointer;
import javax.json.JsonReader;
import javax.json.JsonString;
import javax.json.JsonStructure;
import javax.json.JsonValue;
import javax.json.stream.JsonGenerator;
import javax.security.enterprise.SecurityContext;
import org.bson.BsonDocument;
import org.bson.BsonDateTime;
import org.bson.BsonInt32;
import org.bson.BsonString;
import org.bson.BsonValue;
import org.bson.BsonWriter;
import org.bson.Document;
import org.bson.codecs.DocumentCodec;
import org.bson.codecs.EncoderContext;
import org.bson.conversions.Bson;
import org.bson.json.JsonWriter;
import org.bson.types.ObjectId;

/**
 * @author Dmitry Repchevsky
 */

public class OpenEBenchDAO {
    
    protected Authorization authorization;
    private final AbstractOEBSchemaResolver schemaResolver;

    private ObjectDependencyValidator dependencies_validator;
    private Map<String, JsonObjectValidator> validators;
    
    /**
     * @param authorization
     * @param schemaResolver schema locator to validate submitted documents 
     */
    public OpenEBenchDAO(
            final Authorization authorization,
            final AbstractOEBSchemaResolver schemaResolver) {
        
        this.authorization = authorization;
        this.schemaResolver = schemaResolver;
        
        //dependencies_validator = new ObjectDependencyValidator(authorization.staged, schemaLocator.uri);
        validators = new ConcurrentHashMap<>();
    }

   public String getJsonSchemaURI(final String collection) {
        try {
            final JsonSchemaLocator locator = schemaResolver.resolve(collection);
            if (locator != null) {
                return locator.uri.toString();
            }
        } catch (URISyntaxException | MalformedURLException ex) {
            Logger.getLogger(OpenEBenchDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
   }
   
    public JsonObject getJsonSchema(final String collection) {
        final JsonObjectValidator validator = getValidator(getJsonSchemaURI(collection));
        return validator.getJsonSchema();
    }
    
    public Boolean hasObject(final String id,
                             final String collection) {

        return MongoQueries.hasDocument((authorization.sandbox != null ? 
                authorization.sandbox.getCollection(collection) :
                authorization.staged.getCollection(collection)), id);
    }

    public void writeIDs(Writer writer, String collection) {
        try {
            final HashSet ids = new HashSet();
            
            final MongoCollection<Document> col = authorization.sandbox != null ? 
                    authorization.sandbox.getCollection(collection) :
                    authorization.staged.getCollection(collection);
            
            FindIterable<Document> iterator = col.find()
                    .projection(Projections.include("_id"));
            try (MongoCursor<Document> cursor = iterator.iterator()) {
                while (cursor.hasNext()) {
                    try {
                        final Document doc = cursor.next();
                        final Object _id = doc.get("_id");
                        if (_id instanceof String) {
                            writer.write(_id.toString());
                            writer.write('\n');
                        } else if (_id instanceof Document) {
                            final String id = ((Document)_id).getString("id");
                            if (!ids.contains(id)) {
                                ids.add(id);
                                writer.write(id);
                                writer.write('\n');
                            }
                            final Integer revision = ((Document)_id).getInteger("revision");
                            if (revision != null) {
                                writer.write(id);
                                writer.write('.');
                                writer.write(revision.toString());
                                writer.write('\n');
                            }
                        } else {
                            Logger.getLogger(OpenEBenchDAO.class.getName()).log(Level.WARNING, "invalid primary key: {0}", _id);
                        }
                    } catch(Exception ex) {
                        Logger.getLogger(OpenEBenchDAO.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        } catch(Exception ex) {
            Logger.getLogger(OpenEBenchDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void findContactsByEmail(Writer writer, String email) {
        try {
            final MongoCollection<Document> col = authorization.sandbox != null ? 
                    authorization.sandbox.getCollection(OpenEBenchCollection.CONTACT) :
                    authorization.staged.getCollection(OpenEBenchCollection.CONTACT);

            try (JsonWriter jwriter = new ReusableJsonWriter(writer)) {

                final DocumentCodec codec = new DocumentCodec() {
                    @Override
                    public void encode(BsonWriter writer,
                       Document document,
                       EncoderContext encoderContext) {
                            super.encode(jwriter, document, encoderContext);
                    }
                };
                jwriter.writeStartArray();
                
                FindIterable<Document> iterator = email == null || email.isEmpty() 
                        ? col.find() : col.find(Filters.eq("email", email));

                try (MongoCursor<Document> cursor = iterator.iterator()) {
                    while (cursor.hasNext()) {
                        final Document doc = cursor.next();
                        doc.toJson(codec);
                    }
                }
                jwriter.writeEndArray();
            }
        } catch(Exception ex) {
            Logger.getLogger(OpenEBenchDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Find and write OpenEBench object to the provided StringBuilder.
     * 
     * @param sb a placeholder to put the result
     * @param id the identifier of the OpenEBenc object
     * @param collection OpenEBench collection where to search the object
     * @param privileges
     * @param sc SecuryContext object to check permissions
     * 
     * @return HTTP Status code (200, 400, 401, 403, 404 and 500)
     */
    public int writeObject(final StringBuilder sb, final String id, final String collection, 
            final boolean privileges, final SecurityContext sc) {
        return writeObject(sb, id, collection, null, privileges, sc);
    }

    public int writeObject(final StringBuilder sb, final String id, final String collection, 
            final String path, final boolean privileges, final SecurityContext sc) {
        
        final AbstractAuthorization authz = authorization.getAuthorization(sc, collection, privileges);
        if (authz == null) {
            return HttpURLConnection.HTTP_UNAUTHORIZED;
        }

        try {
            final MongoCollection<Document> col = authorization.sandbox != null ? 
                    authorization.sandbox.getCollection(collection) :
                    authorization.staged.getCollection(collection);
            
            final FindIterable<Document> iter = MongoQueries.findDocument(col, id);
            try (CollectionAuthorizationFilter iterator = new CollectionAuthorizationFilter(authz, iter)) {
                if (iterator.hasNext()) {
                    final Document doc = iterator.next();
                    if (doc != null) {
                        // for provisional database users only access their own data
                        if (authorization.sandbox != null) {
                            final Principal principal = sc.getCallerPrincipal();
                            if (principal == null) {
                                return HttpURLConnection.HTTP_UNAUTHORIZED;
                            }
                            final Document prov = doc.get(OpenEBenchMeta.PROVENANCE_PROPERTY, Document.class);
                            if (prov == null || !principal.getName().equals(doc.getString(OpenEBenchMeta.ORCID_PROPERTY))) {
                                return HttpURLConnection.HTTP_UNAUTHORIZED;
                            }
                        }
                        
                        if (path == null || path.isEmpty()) {
                            // write entire document
                            final Object _id = doc.get("_id");
                            if (_id instanceof Document) {
                                final Document pk = ((Document)_id);
                                doc.put("_id", pk.getString("id"));
                                final Integer revision = pk.getInteger("revision");
                                if (revision != null) {
                                    doc.append(OpenEBenchMeta.REVISION_PROPERTY, revision);
                                }
                            }
//                            doc.remove(OpenEBenchMeta.PROVENANCE_PROPERTY);
                            
                            sb.append(doc.toJson());
                            
                            return HttpURLConnection.HTTP_OK;
                        }

                        JsonPointer pointer;
                        try {
                            pointer = Json.createPointer(path);
                        } catch(Exception ex) {
                            Logger.getLogger(OpenEBenchDAO.class.getName()).log(Level.INFO, null, ex);
                            return HttpURLConnection.HTTP_BAD_REQUEST;
                        }

                        try (JsonReader reader = Json.createReader(new StringReader(doc.toJson()))) {
                            final JsonStructure structure = reader.read();
                            if (pointer.containsValue(structure)) {
                                // write subdocument
                                sb.append(pointer.getValue(structure).toString());
                                return HttpURLConnection.HTTP_OK;
                            }
                        } catch(JsonException ex) {
                            Logger.getLogger(OpenEBenchDAO.class.getName()).log(Level.INFO, null, ex);
                            return HttpURLConnection.HTTP_INTERNAL_ERROR;
                        }
                        return HttpURLConnection.HTTP_NOT_FOUND;
                    }
                } else if (iter.first() != null) {
                    return HttpURLConnection.HTTP_FORBIDDEN;
                }
            }

            // collection is empty
            return HttpURLConnection.HTTP_NOT_FOUND;
        } catch(Exception ex) {
            Logger.getLogger(OpenEBenchDAO.class.getName()).log(Level.SEVERE, null, ex);
            return HttpURLConnection.HTTP_INTERNAL_ERROR;
        }        
    }

    /**
     * Read MongoDB collection writing it into provided writer. The collection 
     * is filtered according the roles found in the security context.
     * For the 'sandbox' database collection is also filtered by the '_provenance.orcid'
     * property (security principal name).
     * 
     * @param writer
     * @param collection
     * @param privileged only those objects user has privileged access will be written
     * @param sc 
     */
    public void writeObjects(
            final Writer writer, 
            final String collection, 
            final boolean privileged, 
            final SecurityContext sc) {
        
        try {
            final JsonWriter jwriter = new ReusableJsonWriter(writer);
            try {
                jwriter.writeStartArray();

                final DocumentCodec codec = new DocumentCodec() {
                    @Override
                    public void encode(BsonWriter writer,
                       Document document,
                       EncoderContext encoderContext) {
                            super.encode(jwriter, document, encoderContext);
                    }
                };

                final CollectionAuthorizationFilter iterator = getCollectionIterator(collection, null, privileged, sc);
                if (iterator != null) {
                    try (iterator) {
                        while (iterator.hasNext()) {
                            final Document doc = iterator.next();
                            if (doc != null) {
                                // for provisional database users only access their own data
                                if (authorization.sandbox != null) {
                                    final Principal principal = sc.getCallerPrincipal();
                                    if (principal == null) {
                                        continue;
                                    }
                                    final Document prov = doc.get(OpenEBenchMeta.PROVENANCE_PROPERTY, Document.class);
                                    if (prov == null || !principal.getName().equals(doc.getString(OpenEBenchMeta.ORCID_PROPERTY))) {
                                        continue;
                                    }
                                }

                                final Object _id = doc.get("_id");
                                if (_id instanceof Document) {
                                    final Document pk = ((Document)_id);
                                    doc.put("_id", pk.getString("id"));
                                    final Integer revision = pk.getInteger("revision");
                                    if (revision != null) {
                                        doc.append(OpenEBenchMeta.REVISION_PROPERTY, revision);
                                    }
                                }
                                //doc.remove(OpenEBenchMeta.PROVENANCE_PROPERTY);
                                
                                doc.toJson(codec);
                            }
                        }                    
                    }
                }
            } finally {
                jwriter.writeEndArray();
                jwriter.close();
            }
        } catch(Exception ex) {
            Logger.getLogger(OpenEBenchDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public CollectionAuthorizationFilter getCollectionIterator(final String collection, 
            final JsonObject filter, final boolean privileged, final SecurityContext sc) {

        final AbstractAuthorization authz = sc == null ? null 
                : authorization.getAuthorization(sc, collection, privileged);
        if (sc == null || authz != null) {
            try {
                final MongoCollection<Document> col = authorization.sandbox != null ? 
                        authorization.sandbox.getCollection(collection) :
                        authorization.staged.getCollection(collection);
            
                if (col != null) {
                    return MongoFilteredCollectionFactory.getCollectionIterator(
                                                col, filter, authz);
                }
            } catch(Exception ex) {
                Logger.getLogger(OpenEBenchDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }
    
    /**
     * Inserts json object into the mongodb
     * 
     * @param object object to be inserted
     * @param community_id the community where the object is added to.
     * @param sc security object to check the access
     * 
     * @return inserted json object.
     * 
     * @throws ValidationException
     */
    public Map<String, Object> putObject(final JsonObject object, final String community_id,
            final SecurityContext sc) throws ValidationException {
        
        final JsonValue value = object.get("_schema");
        if (value == null) {
            throw new ValidationException("/_schema", Arrays.asList(
                    new ValidationError("no object '_schema' specified")));
        }
        if (value.getValueType() != JsonValue.ValueType.STRING) {
            throw new ValidationException("/_schema", Arrays.asList(
                    new ValidationError("object '_schema' must be a string")));
        }
        
        final URI _schema;
        try {
            _schema = URI.create(((JsonString)value).getString());
        } catch(IllegalArgumentException ex) {
            throw new ValidationException("/_schema", Arrays.asList(
                    new ValidationError("'_schema' must be a valid URI")));
        }
        
        final String[] path = _schema.getPath().split("/");
        final String collection = path[path.length - 1];

        return putObject(collection, object, community_id, sc);
    }

    /**
     * Inserts json object into the mongodb collection
     * 
     * @param collection collection object belongs to.
     * @param object object to be inserted.
     * @param community_id the community where the object is added to.
     * @param sc security object to check the access
     * 
     * @return inserted json object as a map of properties.
     * 
     * @throws ValidationException
     */
    public Map<String, Object> putObject(final String collection, 
            final JsonObject object, final String community_id, final SecurityContext sc) 
            throws ValidationException {
        
        final JsonValue _id = object.get("_id");
        if (_id == null) {
            throw new ValidationException("/_id", Arrays.asList(
                    new ValidationError("no object '_id' found")));

        }
        if (_id.getValueType() != JsonValue.ValueType.STRING) {
            throw new ValidationException("/_id", Arrays.asList(
                    new ValidationError("object '_id' must be a string")));
        }

        final String id = ((JsonString)_id).getString();
        
        final String json = object.toString();
        
        return putObject(collection, id, json, community_id, sc);
    }
    
    /**
     * @param collection collection object belongs to.
     * @param id
     * @param json json object to insert
     * @param community_id the community where the object is added to.
     * @param sc security object to check the access
     * 
     * @return inserted json object as a map of properties.
     * 
     * @throws ValidationException 
     */
    public Map<String, Object> putObject(final String collection, final String id, 
            final String json, final String community_id, final SecurityContext sc) 
            throws ValidationException {

        if (community_id == null) {
            switch(collection) {
                case OpenEBenchCollection.COMMUNITY:
                case OpenEBenchCollection.PRIVILEGE:
                case OpenEBenchCollection.REFERENCE:
                case OpenEBenchCollection.ID_SOLV: break;
                default: throw new ValidationException("/", 
                    Arrays.asList(new ValidationError(String.format("%s: no community code found in the object", collection))));
            }   
        }

        final String username = sc.getCallerPrincipal().getName();
        
        final JsonObjectValidator validator = getValidator(collection);

        final List<ValidationError> errors = new ArrayList<>();
        final String json2 = validator.isValid(errors, community_id, json, username, false, true); // !upsert -> false

        if (!errors.isEmpty()) {
            throw new ValidationException(id, errors);
        }
        
        final String _id = validator.getPrimaryKey(community_id, collection, id, username);
        if (_id == null) {
            Logger.getLogger(OpenEBenchDAO.class.getName()).log(Level.INFO, "invalid id {0}", id);
            throw new ValidationException("/", 
                    Arrays.asList(new ValidationError(String.format("invalid OEB identifier: %s", id))));
        }
        
        boolean is_writable;
        try {
            final MongoCollection<Document> col = authorization.sandbox != null ? 
                    authorization.sandbox.getCollection(collection) :
                    authorization.staged.getCollection(collection);

            final Document doc = Document.parse(json2);
            
            final BsonValue pk = createPK(_id, collection);
            doc.put("_id", pk);

            final Document prov = doc.get(OpenEBenchMeta.PROVENANCE_PROPERTY, Document.class);
            if (prov != null) {
                // remove dates to prevent direct injection
                prov.remove(OpenEBenchMeta.CREATED_PROPERTY);
                prov.remove(OpenEBenchMeta.UPDATED_PROPERTY);
            }
            
            if (authorization.sandbox != null || !sc.isCallerInRole(OEBRoles.ADMIN)) {
                doc.append(OpenEBenchMeta.PROVENANCE_PROPERTY, 
                        new BasicDBObject(OpenEBenchMeta.ORCID_PROPERTY, username));
            }

            if (!_id.equals(id)) {
                doc.append("orig_id", id);
            }
            
            if (is_writable = authorization.getAuthorization(sc, collection, true).isWritable(doc)) {

                final List<Bson> updates = new ArrayList();
                updates.add(new BasicDBObject("$set", doc));
                updates.add(new BasicDBObject("$currentDate", new BasicDBObject(
                        OpenEBenchMeta.PROVENANCE_PROPERTY + "." + OpenEBenchMeta.UPDATED_PROPERTY, true)));
                updates.add(new BasicDBObject("$setOnInsert", new BasicDBObject(
                        OpenEBenchMeta.PROVENANCE_PROPERTY + "." + OpenEBenchMeta.CREATED_PROPERTY, 
                        new BsonDateTime(System.currentTimeMillis()))));

                // ???
//                if (authorization.sandbox == null && 
//                    sc.isCallerInRole(OEBRoles.ADMIN) && 
//                    doc.get(OpenEBenchMeta.PROVENANCE_PROPERTY) == null) {
//
//                    updates.add(new BasicDBObject("$set", 
//                            new BasicDBObject(OpenEBenchMeta.PROVENANCE_PROPERTY,
//                                new BasicDBObject("$cond",
//                                    Arrays.asList(new BasicDBObject("$not", 
//                                        new BsonString("$" + OpenEBenchMeta.PROVENANCE_PROPERTY)),
//                                        new BsonString(username), 
//                                        new BsonString("$" + OpenEBenchMeta.PROVENANCE_PROPERTY))))));
//                }

                final FindOneAndUpdateOptions opt = new FindOneAndUpdateOptions().upsert(true)
                                        .returnDocument(ReturnDocument.AFTER);
                
                final Document result = col.findOneAndUpdate(Filters.eq("_id", pk), Updates.combine(updates), opt);            
                if (result != null) {
                    final String canonical_id = OpenEBenchID.getOpenEBenchID(pk);
                    result.put("_id", canonical_id);

                    final Integer revision = OpenEBenchID.getRevision(pk);
                    if (revision != null) {
                        result.append(OpenEBenchMeta.REVISION_PROPERTY, revision);
                    }

                    //result.remove(OpenEBenchMeta.PROVENANCE_PROPERTY);
                    return result;
                }
            }
        } catch(MongoWriteException ex) {
            if (ErrorCategory.DUPLICATE_KEY == ErrorCategory.fromErrorCode(ex.getCode() )) {
                throw new ValidationException("/", 
                        Arrays.asList(new ValidationError(String.format(PKValidationError.MESSAGE, id))));
            }
            Logger.getLogger(OpenEBenchDAO.class.getName()).log(Level.SEVERE, null, ex);
            throw new ValidationException("/", 
                    Arrays.asList(new ValidationError(ex.getMessage())));
        } catch(Exception ex) {
            Logger.getLogger(OpenEBenchDAO.class.getName()).log(Level.SEVERE, null, ex);
            throw new ValidationException("/", 
                    Arrays.asList(new ValidationError(ex.getMessage())));
        }
        
        if (!is_writable) {
            throw new ValidationException("/", 
                Arrays.asList(new ValidationError(String.format("forbidden write %s for %s", id, username))));
        }
        
        return null;
    }
    
    public String patchObject(final String collection,
                              final String path, 
                              final String json,
                              final String community_id,
                              final SecurityContext sc) throws ValidationException {
        
        final AbstractAuthorization authz = authorization.getAuthorization(sc, collection, true);
        if (authz == null) {
            return null; // TODO ?
        }

        final String username = sc.getCallerPrincipal().getName();

        final int path_start;
        switch(collection) {
            case OpenEBenchCollection.PRIVILEGE:
            case OpenEBenchCollection.REFERENCE:
            case OpenEBenchCollection.ID_SOLV: path_start = 0; break;
            case OpenEBenchCollection.COMMUNITY:
                if (!path.matches("^OEBC[0-9]{3}")) {
                    return null;
                }
                path_start = Math.max(path.indexOf("/", 7), 7);
                break;

            default:
                if (!path.matches("^OEB[A-Z]{1}[0-9]{3}[0-9A-Zt]{1}[0-9A-Z]{6}\\S*")) {
                    return null;
                }
                path_start = Math.max(path.indexOf("/", 14), 14);
        }

        final JsonObjectValidator validator = getValidator(getJsonSchemaURI(collection));

        // here we don't need to validate, but rather replace foreign 
        final List<ValidationError> errors = new ArrayList<>();
        final String json_fixed = validator.isValid(errors, community_id, json, username, false, true); // !upsert -> false

        errors.clear();
        
        try {
            final MongoCollection<Document> col = authorization.sandbox != null ? 
                    authorization.sandbox.getCollection(collection) :
                    authorization.staged.getCollection(collection);

            final Bson filter = path_start == 0 ?
                MongoQueries.getOpenEBenchIdFilter(path, true) :
                MongoQueries.getOpenEBenchIdFilter(path.substring(0, path_start));

            final FindIterable<Document> iter = col.find(filter);
            try (CollectionAuthorizationFilter iterator = new CollectionAuthorizationFilter(authz, iter)) {
                if (iterator.hasNext()) {
                    final Document orig = iterator.next();

                    final JsonObject json_orig = Json.createReader(new StringReader(orig.toJson())).readObject();

                    final Object pk = orig.get("_id");
                    final String _id = OpenEBenchID.getOpenEBenchID(pk);
                    final String jpath = path.substring(path_start == 0 ? _id.length() : path_start);

                    final JsonValue fixed_object = Json.createReader(new StringReader(json_fixed)).readValue();

                    final JsonObject jo;
                    if (jpath.length() > 1) {
                        JsonPatch patch;
                        try {
                            Json.createPointer(jpath).getValue(json_orig);
                            patch = Json.createPatchBuilder().replace(jpath, fixed_object).build();
                        } catch (JsonException ex) {
                            // not found
                            patch = Json.createPatchBuilder().add(jpath, fixed_object).build();
                        }

                        jo = patch.apply(json_orig);
                    } else {
                        final JsonMergePatch patch = Json.createMergePatch(fixed_object);
                        jo = patch.apply(json_orig).asJsonObject();
                    }
                    BasicDBObject doc = BasicDBObject.parse(jo.toString());

                    doc.put("_id", pk);

                    doc.remove(OpenEBenchMeta.CREATED_PROPERTY);
                    doc.remove(OpenEBenchMeta.UPDATED_PROPERTY);

                    if (authorization.sandbox != null || !sc.isCallerInRole(OEBRoles.ADMIN)) {
                        doc.append(OpenEBenchMeta.PROVENANCE_PROPERTY, 
                                new BasicDBObject(OpenEBenchMeta.ORCID_PROPERTY, username));
                    }          

                    final List<Bson> bson = new ArrayList();
                    bson.add(new BasicDBObject("$set", doc));
                    bson.add(new BasicDBObject("$currentDate", new BasicDBObject(
                            OpenEBenchMeta.PROVENANCE_PROPERTY + "." + OpenEBenchMeta.UPDATED_PROPERTY, true)));
                    bson.add(new BasicDBObject("$setOnInsert", new BasicDBObject(
                            OpenEBenchMeta.PROVENANCE_PROPERTY + "." + OpenEBenchMeta.CREATED_PROPERTY, 
                            new BsonDateTime(System.currentTimeMillis()))));

                    // ???
//                    if (sc.isCallerInRole(OEBRoles.ADMIN) && doc.get(OpenEBenchMeta.PROVENANCE_PROPERTY) == null) {
//                        bson.add(new BasicDBObject("$set", 
//                                    new BasicDBObject(OpenEBenchMeta.PROVENANCE_PROPERTY,
//                                        new BasicDBObject("$cond",
//                                            Arrays.asList(new BasicDBObject("$not", 
//                                                new BsonString("$" + OpenEBenchMeta.PROVENANCE_PROPERTY)),
//                                                new BsonString(username), 
//                                                new BsonString("$" + OpenEBenchMeta.PROVENANCE_PROPERTY))))));
//                    }

                    final FindOneAndUpdateOptions opt = new FindOneAndUpdateOptions().upsert(true)
                                                .returnDocument(ReturnDocument.AFTER);

                    final Document result = col.findOneAndUpdate(Filters.eq("_id", pk), bson, opt);

                    if (result != null) {
        //                final String canonical_id = OpenEBenchID.getOpenEBenchID(pk);
        //                result.put("_id", canonical_id);

                        final String bare_id = OpenEBenchID.getBareID(pk);
                        result.put("_id", bare_id);

                        final Integer revision = OpenEBenchID.getRevision(pk);
                        if (revision != null) {
                            result.append(OpenEBenchMeta.REVISION_PROPERTY, revision);
                        }

//                        result.remove(OpenEBenchMeta.PROVENANCE_PROPERTY);

                        final String json_result = result.toJson().toString();

                        validator.isValid(errors, community_id, json_result, username, false, true);        
                        if (errors.isEmpty()) {
                            return json_result;
                        }

                        // primitive rollback
                        col.insertOne(orig);
                    }
                }
            }
        } catch(MongoWriteException ex) {
            if (ErrorCategory.DUPLICATE_KEY == ErrorCategory.fromErrorCode(ex.getCode() )) {
                throw new ValidationException("/", 
                        Arrays.asList(new ValidationError(String.format(PKValidationError.MESSAGE, path))));
            }
            Logger.getLogger(OpenEBenchDAO.class.getName()).log(Level.SEVERE, null, ex);
            throw new ValidationException("/", 
                    Arrays.asList(new ValidationError(ex.getMessage())));

        } catch(Exception ex) {
            Logger.getLogger(OpenEBenchDAO.class.getName()).log(Level.SEVERE, null, ex);
            throw new ValidationException("/", 
                    Arrays.asList(new ValidationError(ex.getMessage())));
        }        
        
        if (!errors.isEmpty()) {
            throw new ValidationException(path, errors);
        }
        return null;
    }

    public String hasPayload(final String id, final boolean privileged, final SecurityContext sc) {
        final GridFSBucket bucket = GridFSBuckets.create(authorization.staged, "payloads");
        final GridFSFindIterable iterable = bucket.find(Filters.eq("filename", id));
        try (MongoCursor<GridFSFile> cursor = iterable.cursor()) {
            if (cursor.hasNext()) {
                final GridFSFile file = cursor.next();
                final Document meta = file.getMetadata();
                final PayloadAuthorization auth = new PayloadAuthorization(authorization, privileged, sc);
                if (auth.isReadable(meta)) {
                    return meta == null ? "" : meta.toJson();
                }
                
                return new Document("filename", id).toJson();
            }
        } catch (Exception ex) {}
        
        return null;
    }
    
    public String putPayload(final String id, final InputStream in, 
            final String metadata, final boolean privileged, final SecurityContext sc) {
        
        final Principal principal = sc.getCallerPrincipal();
        if (principal != null) {
            final Document meta = metadata == null || metadata.isEmpty() ? new Document() : Document.parse(metadata);
            final PayloadAuthorization auth = new PayloadAuthorization(authorization, privileged, sc);
            if (auth.isWritable(meta)) {
                meta.append(OpenEBenchMeta.PROVENANCE_PROPERTY, principal.getName());
                final GridFSBucket bucket = GridFSBuckets.create(authorization.staged, "payloads");
                final GridFSUploadOptions options = new GridFSUploadOptions().metadata(meta);
                try {
                    final ObjectId object_id = bucket.uploadFromStream(id, in, options);
                } catch (Exception ex) {
                    
                }
                return meta.toJson();
            }
        }
        return null;
    }

    public InputStream getPayloadStream(Map<String, String> headers, final String id, 
            final boolean privileged, final SecurityContext sc) {

        final GridFSBucket bucket = GridFSBuckets.create(authorization.staged, "payloads");
        final GridFSFindIterable iterable = bucket.find(Filters.eq("filename", id));
        final GridFSFile file = iterable.first();
        if (file != null) {
            final ZonedDateTime upl = ZonedDateTime.ofInstant(file.getUploadDate().toInstant(), ZoneId.systemDefault());
            headers.put("Last-Modified", upl.format(DateTimeFormatter.RFC_1123_DATE_TIME));
            
            final String since = headers.get("If-Modified-Since");
            if (since != null) {
                try {
                    final ZonedDateTime zdt = ZonedDateTime.parse(since, DateTimeFormatter.RFC_1123_DATE_TIME);
                    if (zdt.isAfter(upl)) {
                        return null;
                    }
                } catch (DateTimeParseException ex) {}
            }
            
            final Document metadata = file.getMetadata();
            final PayloadAuthorization auth = new PayloadAuthorization(authorization, privileged, sc);
            if (auth.isReadable(metadata)) {
                if (metadata != null) {
                    metadata.append("@uploadDate", upl.format(DateTimeFormatter.ISO_DATE_TIME));
                    headers.put("X-OEB-METADATA", metadata.toJson());
                }
                
                try {
                    return bucket.openDownloadStream(id);
                } catch (Exception ex) {
                    Logger.getLogger(OpenEBenchDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }   
        }
        return null;
    }

    /**
     * Writes all user data as a json array.
     * 
     * @param writer
     * @param privileged
     * @param sc
     */
    public void writeObjects(final Writer writer, final boolean privileged, final SecurityContext sc) {
        
        try {
            final JsonWriter jwriter = new ReusableJsonWriter(writer);
            try {
                jwriter.writeStartArray();
                
                final Principal principal = sc.getCallerPrincipal();
                if (principal != null) {
                    final DocumentCodec codec = new DocumentCodec() {
                        @Override
                        public void encode(BsonWriter writer,
                           Document document,
                           EncoderContext encoderContext) {
                                super.encode(jwriter, document, encoderContext);
                        }
                    };

                    final MongoIterable<String> collections = authorization.sandbox.listCollectionNames();
                    try (MongoCursor<String> cursor = collections.iterator()) {
                        final JsonObject filter = Json.createObjectBuilder().add(
                                OpenEBenchMeta.PROVENANCE_PROPERTY + "." + OpenEBenchMeta.ORCID_PROPERTY,
                                principal.getName()).build();
                        while (cursor.hasNext()) {
                            final String collection = cursor.next();
                            if (OpenEBenchCollection.isOpenEBenchCollection(collection)) {
                                final CollectionAuthorizationFilter iterator = getCollectionIterator(collection, filter, privileged, sc);
                                if (iterator != null) {
                                    try (iterator) {
                                        while (iterator.hasNext()) {
                                            final Document doc = iterator.next();
                                            if (doc != null) {
//                                                doc.remove(OpenEBenchMeta.PROVENANCE_PROPERTY);
                                                doc.toJson(codec);
                                            }
                                        }                    
                                    }
                                }
                            }
                        }
                    }
                }
            } finally {
                jwriter.writeEndArray();
                jwriter.close();
            }
        } catch(Exception ex) {
            Logger.getLogger(OpenEBenchDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Remove all data that belongs to the user (sandbox)
     * 
     * @param sc 
     */
    public void drop(final SecurityContext sc) {
        try {
            final Principal principal = sc.getCallerPrincipal();
            if (principal != null) {
                final String user_id = principal.getName();
                final Bson filter = Filters.eq(
                        OpenEBenchMeta.PROVENANCE_PROPERTY + "." + OpenEBenchMeta.ORCID_PROPERTY, 
                        user_id);
                final MongoIterable<String> collections = authorization.sandbox.listCollectionNames();
                try (MongoCursor<String> cursor = collections.iterator()) {
                    while (cursor.hasNext()) {
                        final String collection = cursor.next();
                        if (OpenEBenchCollection.isOpenEBenchCollection(collection)) {
                            final MongoCollection<Document> col = authorization.sandbox.getCollection(collection);
                            col.deleteMany(filter);
                        }
                    }
                }
            }
        } catch(Exception ex) {
            Logger.getLogger(OpenEBenchDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Boolean removeObject(final Writer writer,
                                final String id, 
                                final String collection,
                                final SecurityContext sc) {
        
        final AbstractAuthorization authz = authorization.getAuthorization(sc, collection, true);
        if (authz == null) {
            return null;
        }

        final List<DependencyValidationError> err = new ArrayList();
        dependencies_validator.getDependencies(collection, id, err);
        
        if (err.size() > 0) {
            final Map<String, List<DependencyValidationError>> objects = err.stream().collect(Collectors.groupingBy(obj -> obj._id));
            try(JsonGenerator jgenerator = Json.createGeneratorFactory(
                        Collections.singletonMap(JsonGenerator.PRETTY_PRINTING, true))
                        .createGenerator(writer)) {
                
                jgenerator.writeStartArray();

                for (Map.Entry<String, List<DependencyValidationError>> entry : objects.entrySet()) {
                    final List<DependencyValidationError> errors = entry.getValue();

                    jgenerator.writeStartObject();
                    jgenerator.write("_id", entry.getKey());
                    jgenerator.write("_schema", errors.get(0)._schema);

                    jgenerator.writeStartArray("errors");
                    for (DependencyValidationError error : errors) {
                        jgenerator.writeStartObject();
                        jgenerator.write("pointer", error.pointer);
                        jgenerator.write("message", String.format(MESSAGE, id));
                        jgenerator.writeEnd();
                    }
                    jgenerator.writeEnd();

                    jgenerator.writeEnd(); // object
                }            
                jgenerator.writeEnd();
            }
            return false;
        }

        if (sc.isCallerInRole(OEBRoles.ADMIN)) {
            try {
                final MongoCollection<Document> col = authorization.staged.getCollection(collection);
                final DeleteResult result = col.deleteOne(Filters.and(Filters.eq("_id", id), 
                        Filters.eq(OpenEBenchMeta.PROVENANCE_PROPERTY + "." + OpenEBenchMeta.ORCID_PROPERTY,
                                sc.getCallerPrincipal().getName())));

                return result.getDeletedCount() > 0;
            } catch(Exception ex) {
                Logger.getLogger(OpenEBenchDAO.class.getName()).log(Level.SEVERE, null, ex);
            }        
        }        
        return null;
    }

    protected JsonObjectValidator getValidator(String collection) {
        JsonObjectValidator validator = validators.get(collection);
        if (validator == null) {
            try {
                final JsonSchemaLocator locator = schemaResolver.getJsonSchemaLocator(collection);
                validator = new JsonObjectValidator(locator, authorization);
                validators.put(collection, validator);
            } catch(Exception ex) {
                Logger.getLogger(OpenEBenchDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return validator;

    }
    
    private BsonValue createPK(String id, String collection) {
        
        if (authorization.sandbox != null) {
            return new BsonString(id); // sandbox PK is always a string
        }

        switch(collection) {
            case OpenEBenchCollection.CONTACT:
            case OpenEBenchCollection.PRIVILEGE:
            case OpenEBenchCollection.COMMUNITY:
            case OpenEBenchCollection.REFERENCE: return new BsonString(id);
        }
        
        final int idx = id.lastIndexOf('.');
        if (idx >= 0) {
            final String suffix = id.substring(idx + 1);
            try {
                final int revision = Integer.parseUnsignedInt(suffix);
                return new BsonDocument("id", new BsonString(id.substring(0, idx)))
                        .append("revision", new BsonInt32(revision));
            } catch (NumberFormatException ex) {
                Logger.getLogger(OpenEBenchDAO.class.getName()).log(Level.WARNING, "invalid revision: {0}", id);
            }
        } else {
            final MongoCollection<BsonDocument> col = authorization.staged.getCollection(collection, BsonDocument.class);
            final BsonDocument last = MongoQueries.findDocument(col, id).first();
            return new BsonDocument("id", new BsonString(id))
                        .append("revision", new BsonInt32(last == null ? 1 : last.getInt32("revision", new BsonInt32(0)).getValue() + 1));
        }
        
        return new BsonString(id);
    }
}
