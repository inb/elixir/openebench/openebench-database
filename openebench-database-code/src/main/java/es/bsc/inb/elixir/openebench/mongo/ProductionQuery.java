/**
 * *****************************************************************************
 * Copyright (C) 2020 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.mongo;

import com.mongodb.BasicDBObject;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import es.bsc.inb.elixir.openebench.auth.Authorization;
import es.bsc.inb.elixir.openebench.auth.CollectionAuthorizationFilter;
import es.bsc.inb.elixir.openebench.auth.OEBRoles;
import java.io.StringReader;
import java.io.Writer;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.security.enterprise.SecurityContext;
import org.bson.Document;

/**
 * @author Dmitry Repchevsky
 */

public class ProductionQuery {
    
    private final Authorization authorization;
        
    public ProductionQuery(Authorization authorization) {
        this.authorization = authorization;
    }

    public JsonObject getContactsForCommunity(final String community_id, final SecurityContext sc) {
       
        final JsonObjectBuilder builder = Json.createObjectBuilder();
        
        try {
            final MongoDatabase mdb = authorization.staged;
            final MongoCollection<Document> privilege_collection = mdb.getCollection(OpenEBenchCollection.PRIVILEGE);
            final MongoCollection<Document> collection = mdb.getCollection(OpenEBenchCollection.COMMUNITY);

            try (MongoCursor<Document> communities = collection.find(Filters.eq("_id", community_id)).iterator()) {
                if (communities.hasNext()) {
                    try (MongoCursor<Document> privileges = privilege_collection.find().iterator()) {
                        final JsonArrayBuilder owners_builder = Json.createArrayBuilder();
                        while (privileges.hasNext()) {
                            final Document privilege = privileges.next();
                            if (privilege != null) {
                                final List<Document> roles = privilege.get("roles", List.class);
                                for (Document role : roles) {
                                    final String r = role.getString("role");
                                    if ((community_id.equals(role.getString("community_id")) &&
                                         OEBRoles.OWNER.equals(r))) {
                                        owners_builder.add(privilege.getString("_id"));
                                    }
                                }
                            }
                        }
                        final JsonArray owners = owners_builder.build();
                        if (!owners.isEmpty()) {
                            builder.add(OEBRoles.OWNER, owners);
                        }
                    }
                }
                return builder.build();
            }
        } catch(Exception ex) {
            Logger.getLogger(ProductionQuery.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }

    public JsonObject getContactsForBenchmarkingEvent(final String benchmarking_event_id, final SecurityContext sc) {
       
        final JsonObjectBuilder builder = Json.createObjectBuilder();
        
        try {
            final MongoDatabase mdb = authorization.staged;
            final MongoCollection<Document> privileges_collection = mdb.getCollection(OpenEBenchCollection.PRIVILEGE);
            final MongoCollection<Document> events_collection = mdb.getCollection(OpenEBenchCollection.BENCHMARKING_EVENT);

            try (MongoCursor<Document> events = MongoQueries.findDocument(
                                                        events_collection, benchmarking_event_id).iterator()) {
                if (events.hasNext()) {
                    final Document event = events.next();
                    final String community_id = event != null ? event.getString("community_id") : null;
                    
                    try (MongoCursor<Document> privileges = privileges_collection.find().iterator()) {
                        final JsonArrayBuilder managers_builder = Json.createArrayBuilder();
                        final JsonArrayBuilder owners_builder = Json.createArrayBuilder();
                        
                        while (privileges.hasNext()) {
                            final Document privilege = privileges.next();
                            if (privilege != null) {
                                final List<Document> roles = privilege.get("roles", List.class);
                                for (Document role : roles) {
                                    final String r = role.getString("role");
                                    if (benchmarking_event_id != null &&
                                        benchmarking_event_id.equals(role.getString("benchmarking_event_id")) &&
                                        OEBRoles.MANAGER.equals(r)) {
                                        managers_builder.add(privilege.getString("_id"));
                                        continue;
                                    }
                                    if (community_id != null &&
                                        community_id.equals(role.getString("community_id")) &&
                                        OEBRoles.OWNER.equals(r)) {
                                        owners_builder.add(privilege.getString("_id"));
                                        continue;
                                    }
                                }
                            }
                        }

                        final JsonArray managers = managers_builder.build();
                        final JsonArray owners = owners_builder.build();
                        
                        if (!managers.isEmpty()) {
                            builder.add(OEBRoles.MANAGER, managers);
                        }
                        if (!owners.isEmpty()) {
                            builder.add(OEBRoles.OWNER, owners);
                        }
                    }
                }
                return builder.build();
            }
        } catch(Exception ex) {
            Logger.getLogger(ProductionQuery.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }

    public JsonObject getContactsForChallenge(final String challenge_id, final SecurityContext sc) {
       
        final JsonObjectBuilder builder = Json.createObjectBuilder();
        
        try {
            final MongoDatabase mdb = authorization.staged;
            final MongoCollection<Document> privileges_collection = mdb.getCollection(OpenEBenchCollection.PRIVILEGE);
            final MongoCollection<Document> challenges_collection = mdb.getCollection(OpenEBenchCollection.CHALLENGE);
            final MongoCollection<Document> events_collection = mdb.getCollection(OpenEBenchCollection.BENCHMARKING_EVENT);

            try (MongoCursor<Document> challenges = MongoQueries.findDocument(
                                                        challenges_collection, challenge_id).iterator()) {
                if (challenges.hasNext()) {
                    final Document challenge = challenges.next();
                    final String benchmarking_event_id = challenge.getString("benchmarking_event_id");
                    final Document event = MongoQueries.findDocument(events_collection, benchmarking_event_id).first();
                    final String community_id = event != null ? event.getString("community_id") : null;
                    
                    try (MongoCursor<Document> privileges = privileges_collection.find().iterator()) {
                        final JsonArrayBuilder supervisors_builder = Json.createArrayBuilder();
                        final JsonArrayBuilder managers_builder = Json.createArrayBuilder();
                        final JsonArrayBuilder owners_builder = Json.createArrayBuilder();
                        
                        while (privileges.hasNext()) {
                            final Document privilege = privileges.next();
                            if (privilege != null) {
                                final List<Document> roles = privilege.get("roles", List.class);
                                for (Document role : roles) {
                                    final String r = role.getString("role");
                                    if ((challenge_id.equals(role.getString("challenge_id")) &&
                                         OEBRoles.CONTRIBUTOR.equals(r))) {
                                        supervisors_builder.add(privilege.getString("_id"));
                                        continue;
                                    }
                                    if (benchmarking_event_id != null &&
                                        benchmarking_event_id.equals(role.getString("benchmarking_event_id")) &&
                                        OEBRoles.MANAGER.equals(r)) {
                                        managers_builder.add(privilege.getString("_id"));
                                        continue;
                                    }
                                    if (community_id != null &&
                                        community_id.equals(role.getString("community_id")) &&
                                        OEBRoles.OWNER.equals(r)) {
                                        owners_builder.add(privilege.getString("_id"));
                                        continue;
                                    }
                                }
                            }
                        }

                        final JsonArray managers = managers_builder.build();
                        final JsonArray owners = owners_builder.build();
                        
                        if (!managers.isEmpty()) {
                            builder.add(OEBRoles.MANAGER, managers);
                        }
                        if (!owners.isEmpty()) {
                            builder.add(OEBRoles.OWNER, owners);
                        }
                    }
                }
                return builder.build();
            }
        } catch(Exception ex) {
            Logger.getLogger(ProductionQuery.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    public void getAggregationDatasets(final String dataset_id, 
            final Writer writer, final SecurityContext sc) {
        
        try {
            final MongoDatabase mdb = authorization.staged;
            final MongoCollection<Document> datasets = mdb.getCollection(OpenEBenchCollection.DATASET);           

            final FindIterable<Document> datasets_iterable = MongoQueries.findDocument(datasets, dataset_id);
            try (CollectionAuthorizationFilter dataset_iterator = 
                    new CollectionAuthorizationFilter(authorization.getDatasetAuthorization(false, sc), datasets_iterable)) {
                if (dataset_iterator.hasNext()) {
                    final Document dataset = dataset_iterator.next();
                    final List<String> challenge_ids = dataset.get("challenge_ids", List.class);
                    if (challenge_ids != null) {
                        final MongoCollection<Document> challenges = mdb.getCollection(OpenEBenchCollection.CHALLENGE);
                        final FindIterable<Document> challenges_iterable = challenges.find(Filters.eq("_id", dataset_id));
                        try (CollectionAuthorizationFilter challenge_iterator = 
                                new CollectionAuthorizationFilter(authorization.getChallengeAuthorization(false, sc),challenges_iterable)) {
                            if (challenge_iterator.hasNext()) {
                                final Document challenge = challenge_iterator.next();
                            }
                        }
                    }
                }                    
            }
        } catch(Exception ex) {
            Logger.getLogger(ProductionQuery.class.getName()).log(Level.SEVERE, null, ex);
        } 
        
    }
    
    public Collection<JsonObject> getToolsForBenchmarkingEvent(final String event_id, List<String> projections, final SecurityContext sc) {
               
        final Map<String, JsonObject> map = new HashMap();
        try {
            final MongoDatabase mdb = authorization.staged;
            final MongoCollection<Document> challenges = mdb.getCollection(OpenEBenchCollection.CHALLENGE);
            final MongoCollection<Document> datasets = mdb.getCollection(OpenEBenchCollection.DATASET);
            final MongoCollection<Document> tools = mdb.getCollection(OpenEBenchCollection.TOOL);

            final FindIterable<Document> challenges_iterable = challenges.find(Filters.eq("benchmarking_event_id", event_id));
            try (CollectionAuthorizationFilter challenges_iterator = 
                    new CollectionAuthorizationFilter(authorization.getChallengeAuthorization(false, sc), challenges_iterable)) {
                while (challenges_iterator.hasNext()) {
                    final Document challenge = challenges_iterator.next();
                    final String challenge_id = challenge.getString("_id");
                    final FindIterable<Document> datasets_iterable = datasets.find(
                            Filters.and(Filters.eq("type", "participant"), 
                                        Filters.eq("challenge_ids", challenge_id)));
                    try (CollectionAuthorizationFilter datasets_iterator = 
                        new CollectionAuthorizationFilter(authorization.getDatasetAuthorization(false, sc), datasets_iterable)) {
                        while (datasets_iterator.hasNext()) {
                            final Document dataset = datasets_iterator.next();
                            final Document depends = dataset.get("depends_on", Document.class);
                            if (depends != null) {
                                final String tool_id = depends.getString("tool_id");
                                if (tool_id != null && !map.containsKey(tool_id)) {
                                    FindIterable<Document> tools_iterable = tools.find(Filters.eq("_id", tool_id));
                                    if (projections != null && !projections.isEmpty()) {
                                        BasicDBObject bson = new BasicDBObject();
                                        for (String field : projections) {
                                            bson.append(field, true);
                                        }
                                        tools_iterable = tools_iterable.projection(bson);
                                    }
                                    final Document tool = tools_iterable.first();
                                    if (tool != null) {
                                        map.put(tool_id, Json.createReader(new StringReader(tool.toJson())).readObject());
                                    }
                                }                            
                            }
                        }
                    }
                }
            }
        } catch(Exception ex) {
            Logger.getLogger(ProductionQuery.class.getName()).log(Level.SEVERE, null, ex);
        } 
        
        return map.values();
    }
    
    public JsonObject getPrivileges(final SecurityContext sc) {
        
        final JsonArrayBuilder jb = Json.createArrayBuilder();
        
        try {
            final MongoDatabase mdb = authorization.staged;
            
            final MongoCollection<Document> communities = mdb.getCollection(OpenEBenchCollection.COMMUNITY);
            final FindIterable<Document> communities_iterable = communities.find().projection(new Document("_id", 1));
            try (CollectionAuthorizationFilter communities_iterator = 
                    new CollectionAuthorizationFilter(authorization.getCommunityAuthorization(false, sc), communities_iterable)) {
                while (communities_iterator.hasNext()) {
                    final Document community = communities_iterator.next();
                    final String community_id = community.getString("_id");
                    if (sc.isCallerInRole(OEBRoles.ADMIN) || sc.isCallerInRole(OEBRoles.OWNER + ":" + community_id)) {
                        jb.add(Json.createObjectBuilder().add(OEBRoles.OWNER, community_id));
                    }
                }
            }
            
            final MongoCollection<Document> benchmarking_events = mdb.getCollection(OpenEBenchCollection.BENCHMARKING_EVENT);
            final FindIterable<Document> benchmarking_events_iterable = benchmarking_events.find().projection(new Document("_id", 1));
            try (CollectionAuthorizationFilter benchmarking_events_iterator = 
                    new CollectionAuthorizationFilter(authorization.getBenchmarkingEventAuthorization(false, sc), benchmarking_events_iterable)) {
                while (benchmarking_events_iterator.hasNext()) {
                    final Document benchmarking_event = benchmarking_events_iterator.next();
                    final String benchmarking_event_id = benchmarking_event.get("_id", Document.class).getString("id");
                    if (sc.isCallerInRole(OEBRoles.MANAGER + ":" + benchmarking_event_id)) {
                        jb.add(Json.createObjectBuilder().add(OEBRoles.MANAGER, benchmarking_event_id));
                    }
                }
            }

        } catch(Exception ex) {
            Logger.getLogger(ProductionQuery.class.getName()).log(Level.SEVERE, null, ex);
        } 
        
        return Json.createObjectBuilder().add("roles", jb).build();
    }
}