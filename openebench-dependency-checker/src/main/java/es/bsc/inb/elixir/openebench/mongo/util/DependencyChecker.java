/**
 * *****************************************************************************
 * Copyright (C) 2021 Spanish National Bioinformatics Institute (INB) and
 * Barcelona Supercomputing Center
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.mongo.util;

import com.mongodb.ConnectionString;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import es.bsc.inb.elixir.openebench.validator.DependencyValidationError;
import static es.bsc.inb.elixir.openebench.validator.DependencyValidationError.MESSAGE;
import es.bsc.inb.elixir.openebench.validator.GitHubSchemaLocator;
import es.bsc.inb.elixir.openebench.validator.ObjectDependencyValidator;
import java.io.File;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.json.Json;
import javax.json.stream.JsonGenerator;

/**
 * @author Dmitry Repchevsky
 */

public class DependencyChecker {
    
    private final static String HELP = "validator -u -p -id [-mongo] [-schema] \n\n" +
                                       "parameters:\n\n" +
                                       "-u             - mongodb user\n" +
                                       "-p             - mongodb password\n" +
                                       "-id            - OEB identifier to check dependencies for \n" +
                                       "-mongo         - MongoDB database connection URI (Optional) @see /META-INF/config.properties \n" +
                                       "-schema        - JSON Schema location URL (Optional) @see /META-INF/config.properties \n" +
                                       "-delete        - DELETE OEB object with ALL refferers (aka 'CASCADE' in SQL)\n\n";
   
    private final static String DEFAULT_MONGODB_URI = 
            "mongodb://";

    static {
        String uri = DEFAULT_MONGODB_URI;
        try {
            try (InputStream in = DependencyChecker.class.getClassLoader().getResourceAsStream("META-INF/config.properties")) {
                if (in != null) {
                    final Properties properties = new Properties();
                    properties.load(in);
                    uri = properties.getProperty("mongodb.uri");
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(DependencyChecker.class.getName()).log(Level.SEVERE, null, ex);
        }
        MONGODB_URI = URI.create(uri);
    }

    public final static URI MONGODB_URI;

    public static void main(String[] args) {
        Map<String, List<String>> params = parameters(args);
        
        if (params.isEmpty()) {
            System.out.println(HELP);
            System.exit(0);
        }

        URI mongo = MONGODB_URI;
        final List<String> lmongo = params.get("-mongo");
        if (lmongo != null && !lmongo.isEmpty()) {
            try {
                mongo = new URI(lmongo.get(0));
            } catch (URISyntaxException ex) {
                System.out.println("invalid -mongo URL");
                System.out.println(HELP);
                System.exit(0);
            }
        }
        
        URI schema = GitHubSchemaLocator.GITHUB_SCHEMA_LOCATION;
        final List<String> lschema = params.get("-schema");
        if (lschema != null && !lschema.isEmpty()) {
            try {
                schema = new URI(lschema.get(0));
            } catch (URISyntaxException ex) {
                System.out.println("invalid -schema URL");
                System.out.println(HELP);
                System.exit(0);
            }
        }
        
        final List<String> ldelete = params.get("-delete");
        
        final List<String> lid = params.get("-id");
        final String id = lid == null || lid.isEmpty() ? null : lid.get(0);

        if (id == null && (ldelete == null || ldelete.isEmpty())) {
            System.out.println("no openebench identifier provided.");
            System.out.println(HELP);
            System.exit(0);
        }
        
        final List<String> luser = params.get("-u");
        final String user = luser == null || luser.isEmpty() ? null : luser.get(0);
        
        final List<String> lpassword = params.get("-p");
        final String password = lpassword == null || lpassword.isEmpty() ? null : lpassword.get(0);
        
        final ConnectionString connection;
        if (user == null || password == null ) {
            if (mongo.getUserInfo() == null) {
                System.out.println("no user credentials provided.");
                System.out.println(HELP);
                System.exit(0);
            }
            connection = new ConnectionString(mongo.toString());
        } else {
            try {
                final URI uri = new URI(mongo.getScheme(),
                        user + ":" + password, mongo.getHost(), mongo.getPort(),
                        mongo.getPath(), mongo.getQuery(), mongo.getFragment());
                connection = new ConnectionString(uri.toString());
            } catch (URISyntaxException ex) {
                System.out.println(ex.getMessage());
                System.out.println(HELP);
                return;
            }
        }

        final MongoClient mc = MongoClients.create(connection);
        final ObjectDependencyValidator validator = 
                new ObjectDependencyValidator(
                        mc.getDatabase(connection.getDatabase()), schema);
        
        if (ldelete == null) {
            checkDelete(validator, id);
        } else if (ldelete.isEmpty()) {
            prepareDelete(validator, id);
        } else {
            commitDelete(validator, ldelete.get(0));
        }
    }
    
    private static void checkDelete(final ObjectDependencyValidator validator, final String id) {
        final List<DependencyValidationError> err = new ArrayList();
        validator.getDependencies(id, err);
        
        final Map<String, List<DependencyValidationError>> objects = err.stream().collect(Collectors.groupingBy(obj -> obj._id));
        
        try (JsonGenerator writer = Json.createGeneratorFactory(Map.of(JsonGenerator.PRETTY_PRINTING, Boolean.TRUE))
                .createGenerator((System.out))) {
            writer.writeStartArray();
            for (Map.Entry<String, List<DependencyValidationError>> entry : objects.entrySet()) {
                final List<DependencyValidationError> errors = entry.getValue();
                writer.writeStartObject();
                writer.write("_id", entry.getKey());
                writer.write("_schema", errors.get(0)._schema);
                
                writer.writeStartArray("errors");
                for (DependencyValidationError error : errors) {
                    writer.writeStartObject();
                    writer.write("pointer", error.pointer);
                    writer.write("message", String.format(MESSAGE, id));
                    writer.writeEnd();
                }
                writer.writeEnd();
                
                writer.writeEnd(); // object
            }                
            writer.writeEnd();
        }        
    }

    private static void prepareDelete(final ObjectDependencyValidator validator, final String id) {
        final Set<String> identifiers = validator.remove(id);
        for (String identifier : identifiers) {
            System.out.println(identifier);
        }
    }
    
    private static void commitDelete(final ObjectDependencyValidator validator, final String file) {
        try {
            final Path path = new File(file).toPath();
            final List<String> identifiers = Files.readAllLines(path);
            final List<String> failed = validator.remove(identifiers);
            for (String identifier : identifiers) {
                System.out.println(identifier);
            }            
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        }
    }
    
    private static Map<String, List<String>> parameters(String[] args) {
        TreeMap<String, List<String>> parameters = new TreeMap();        
        List<String> values = null;
        for (String arg : args) {
            switch(arg) {
                case "-u":
                case "-p":
                case "-id":
                case "-mongo":
                case "-schema":
                case "-delete":
                            values = parameters.get(arg);
                                  if (values == null) {
                                      values = new ArrayList(); 
                                      parameters.put(arg, values);
                                  }
                                  break;
                default: if (values != null) {
                    values.add(arg);
                }
            }
        }
        return parameters;
    }
}
